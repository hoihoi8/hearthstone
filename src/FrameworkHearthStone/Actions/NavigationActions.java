package FrameworkHearthStone.Actions;

import FrameworkHearthStone.HearthstoneEnums.adventureDifficulty;
import FrameworkHearthStone.DataStructures.Enums.classes;
import FrameworkHearthStone.ProcessManagement.StartHearthstone;
import FrameworkHearthStone.Utilities.SleepUtils;

public class NavigationActions extends Actions 
{
	public void loadToMainMenu()
	{
		int loops = 20;
		for(int i=0; i<=loops; i++)
		{
			Framework.Logger.log("Searching for game entry");
			if(hearthstone().screens().seasonRewards().isLoaded() || hearthstone().screens().todaysQuests().isLoaded() 
					|| hearthstone().screens().mainMenu().isLoaded()) {
				Framework.Logger.log("Game Entry point found");
				break;
			}
			if(i == loops) {
				Framework.Validate.fail("Could not find entry point to game");
			}
			SleepUtils.waitRandomTime(3000, 3100);
		}
		
		for(int i=0; i<loops; i++)
		{
			if(hearthstone().screens().seasonRewards().isLoaded()) {
				hearthstone().screens().seasonRewards().handleSeasonRewards();
			}
			if(hearthstone().screens().todaysQuests().isLoaded()) {
				hearthstone().screens().todaysQuests().handleTodaysQuests();
			}
			
			if(hearthstone().screens().mainMenu().isLoaded()) {
				break;
			}
			else if(i == loops)
			{
				Framework.Validate.fail("Could not find Main Menu");
			}
		}
	}
	
	public classes determineClassToPlay()
	{
		classes classToPlay = null;
		hearthstone().screens().mainMenu().clickQuests();
		hearthstone().screens().questLog().verifyLoaded();
		hearthstone().screens().questLog().ditchGarbageQuests();
		classToPlay = hearthstone().screens().questLog().getClassToPlay();
		hearthstone().screens().questLog().clickExit();
		hearthstone().screens().mainMenu().verifyLoaded();
		
		return classToPlay;
	}
	
	public void PlayCasualPvPGame(classes classToPlay, int gameNumber)
	{
		Framework.Logger.log("--------------------Play Casual Game: " + classToPlay.toString() + "--------------------");
		Framework.Logger.log("--------------------Class: " + classToPlay.toString() + " Game Count: " + Integer.toString(gameNumber) + "--------------------");
		
		hearthstone().screens().mainMenu().clickPlay();
		for(int i = 0; i < 5; i++)
		{
			hearthstone().screens().playScreen().verifyLoaded();
			hearthstone().screens().playScreen().clickPlay(classToPlay);
			SleepUtils.waitRandomTime(1000, 1100);
			if(!hearthstone().screens().playScreen().isLoaded())
			{
				break;
			}
		}
		
		hearthstone().screens().mulligan().verifyLoaded(120);
		hearthstone().screens().mulligan().executeMulligan();
		hearthstone().screens().board(classToPlay).verifyLoaded(120);
		hearthstone().screens().board(classToPlay).playOneGame();
		
		while(!hearthstone().screens().playScreen().isLoaded())
		{
			hearthstone().screens().board(classToPlay).gameData.playerBoardRegion.click();
			SleepUtils.waitRandomTime(3000, 4000);
		}
	}
	
	public void PlayAIGame(classes classToPlay, adventureDifficulty difficulty)
	{
		hearthstone().screens().mainMenu().verifyLoaded();
		hearthstone().screens().mainMenu().clickSoloAdventures();
		hearthstone().screens().practiceChooseAdventure().verifyLoaded();
		hearthstone().screens().practiceChooseAdventure().selectDifficulty(difficulty);
		hearthstone().screens().practiceChooseAdventure().clickChoose();
		hearthstone().screens().practiceChooseDeck().verifyLoaded();
		hearthstone().screens().practiceChooseDeck().clickPlay(classToPlay);
		hearthstone().screens().mulligan().verifyLoaded(120);
		hearthstone().screens().mulligan().executeMulligan();
		hearthstone().screens().board(classToPlay).verifyLoaded();
		hearthstone().screens().board(classToPlay).playOneGame();
		
		while(!hearthstone().screens().practiceChooseDeck().isLoaded())
		{
			hearthstone().screens().board(classToPlay).gameData.playerBoardRegion.click();
			SleepUtils.waitRandomTime(3000, 4000);
		}
	}
	
	public void exitGameFromPlayScreen()
	{
		hearthstone().screens().playScreen().verifyLoaded();
		hearthstone().screens().gameHUD().clickSettingsButton();
		hearthstone().screens().playScreen().settings().verifyLoaded();
		hearthstone().screens().playScreen().settings().clickQuitButton();
		StartHearthstone.closeBattleNet();
		SleepUtils.waitRandomTime(5000, 5500);
	}
}

package FrameworkHearthStone.Utilities;

import java.awt.image.BufferedImage;
import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.imageio.ImageIO;

public class ScreenshotUtils {
	public static void takeScreenshot(String fileName)
	{
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd hh mm ss a");
		Calendar now = Calendar.getInstance();
		Robot robot;
		try {
			String builtFileName = fileName + "_" + formatter.format(now.getTime())+".jpg";
        	builtFileName = builtFileName.replaceAll("\\s+","_");
			Framework.Logger.log(FileUtils.outputFile(builtFileName));
			robot = new Robot();
			BufferedImage screenShot = robot.createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
	        try {
	        	
				ImageIO.write(screenShot, "JPG", new File(builtFileName));
			} catch (IOException e) {
				Framework.Logger.log("Saving screenshot failed: " + e.getMessage());
			}
		} catch (AWTException e) {
			Framework.Logger.log(e.getMessage());
		}
	}
}

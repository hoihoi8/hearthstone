package Tests.Screens;

import org.junit.Test;

import FrameworkHearthStone.Screens.Mulligan;
import FrameworkHearthStone.Utilities.WindowUtils;

public class MulliganScreenTest {

	@Test
	public void MulliganExecutesAndConfirms() {
		Mulligan mulligan = new Mulligan(WindowUtils.getGameWindow());
		mulligan.executeMulligan();
	}
}

package Tests.DailyRun;

import org.junit.Test;

import FrameworkHearthStone.DataStructures.AccountData;
import FrameworkHearthStone.DataStructures.Enums.classes;
import FrameworkHearthStone.ProcessManagement.StartHearthstone;
import Tests.BaseTest;

public class PlayDailyGames extends BaseTest
{
	@Test
	public void StartDailyGames()
	{
		Framework.Logger.log("StartDailyGames");
		
		classes classToPlay = null;
		
		for(AccountData account : hearthstone().accounts().accounts)
		{
			StartHearthstone.loadGame(account.accountName, account.password);
			
			hearthstone().actions().navigationActions().loadToMainMenu();
			classToPlay = hearthstone().actions().navigationActions().determineClassToPlay();
			hearthstone().screens().mainMenu().clickPlay();
		
			for(int gameNumber = 0; gameNumber < 5; gameNumber++)
			{
				hearthstone().actions().navigationActions().PlayCasualPvPGame(classToPlay, gameNumber);
			}
			
			hearthstone().actions().navigationActions().exitGameFromPlayScreen();
		}
	}
}

package FrameworkHearthStone.Screens;

import org.sikuli.script.Pattern;
import org.sikuli.script.Region;

import Framework.SikuliWrap;


public class BaseScreen
{
	private String screenName;
	protected Region gameWindow;
	private Pattern rootElement;
	
	public BaseScreen(Pattern rootElement, String screenName, Region gameWindow)
	{
		this.gameWindow = gameWindow;
		this.gameWindow.setAutoWaitTimeout(0);
		this.rootElement = rootElement;
		this.screenName = screenName;
	}
	
	public void verifyLoaded()
	{
		verifyLoaded(30, true);
	}
	public void verifyLoaded(int timeout)
	{
		verifyLoaded(timeout, true);
	}
	public void verifyLoaded(int timeout, boolean logFunction)
	{
		if(logFunction)
			Framework.Logger.log(screenName + ".VerifyLoaded: " + rootElement);
		
		if(!SikuliWrap.wait(gameWindow, rootElement, 30, logFunction))
		{
			Framework.Validate.fail("Screen not loaded: " + screenName + " " + rootElement.toString());
		}
	}
	
	public boolean isLoaded()
	{
		return isLoaded(0, true);
	}
	public boolean isLoaded(int timeout, boolean logFunction)
	{
		if(logFunction)
			Framework.Logger.log(screenName + ".isLoaded: " + rootElement.toString());
		
		boolean isLoaded = false;
		if(SikuliWrap.exists(gameWindow, rootElement.similar(.7f), logFunction) != null)
		{
			isLoaded = true;
		}
		
		return isLoaded;
	}
}

package FrameworkHearthStone.Screens;

import org.sikuli.script.Pattern;
import org.sikuli.script.Region;

import Framework.SikuliWrap;
import FrameworkHearthStone.Utilities.FileUtils;
import FrameworkHearthStone.Utilities.Images;

public class TodaysQuests extends BaseScreen
{
	private static final String imagesFolder = Images.todaysQuestsFolder;
	private static Pattern rootElement = FileUtils.getPattern(imagesFolder, Images.todaysQuests);

	public TodaysQuests(Region gameWindow) {
		super(rootElement, "TodaysQuests", gameWindow);
	}
	
	public void handleTodaysQuests() {
		Framework.Logger.log("Handle today's quests");
		SikuliWrap.clickRandomLocation(FileUtils.getPattern(imagesFolder, Images.todaysQuests), gameWindow);
	}
}
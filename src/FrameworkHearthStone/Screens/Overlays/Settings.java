package FrameworkHearthStone.Screens.Overlays;

import org.sikuli.script.Pattern;
import org.sikuli.script.Region;

import Framework.SikuliWrap;
import FrameworkHearthStone.Screens.BaseScreen;
import FrameworkHearthStone.Utilities.FileUtils;
import FrameworkHearthStone.Utilities.Images;


public class Settings extends BaseScreen {
	private static final String imagesFolder = Images.settingsFolder;
	private static Pattern rootElement = FileUtils.getPattern(imagesFolder, Images.quitGameButton);
	
	public Settings(Region gameWindow) {
		super(rootElement, "Settings", gameWindow);
	}
	
	public void clickQuitButton()
	{
		SikuliWrap.clickRandomLocation(FileUtils.getPattern(imagesFolder, Images.quitGameButton), gameWindow);
	}
}
package FrameworkHearthStone.Utilities;

import java.io.File;

import org.sikuli.script.Pattern;

import Framework.Logger;

public class FileUtils
{
	private static String rootLocation = System.getProperty("user.dir");
	private static String outputImageLocation = rootLocation + "\\Output\\ScreenShots\\";
	private static String imageLocation = rootLocation + "\\Images";
	
	public static String fileName(String fileName)
	{
		String result = rootLocation + fileName;
		if(!new File(result).exists())
		{
			Logger.log("fileName:" + result + " does not exist.");
		}
		return result;
	}
	
	public static Pattern getPattern(String fileName)
	{
		return getPattern("", fileName, .7f);
	}
	
	public static Pattern getPattern(String folder, String fileName)
	{
		return getPattern(folder, fileName, .7f);
	}
	
	public static Pattern getPattern(String folder, String fileName, float score)
    {
		Pattern pattern = new Pattern(imageName(folder, fileName)).similar(score);
    	return pattern;
    }
	
	public static String imageName(String folder, String fileName)
    {
		String result = imageLocation + folder + fileName;
		if(!new File(result).exists())
		{
			Logger.log("ImageName:" + result + " does not exist.");
		}
    	return result;
    }
	
	
	
	public static String outputFile(String fileName)
	{
		return outputImageLocation + fileName;
	}
}

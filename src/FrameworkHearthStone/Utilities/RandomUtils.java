package FrameworkHearthStone.Utilities;

import java.util.Random;

import org.sikuli.script.Location;
import org.sikuli.script.Match;
import org.sikuli.script.Region;

public class RandomUtils {
	public static Location getRandomLocation(Region region)
	{
		return getRandomLocation(region.x, region.y, region.w, region.h);
	}
	public static Location getRandomLocation(Match match)
	{
		return getRandomLocation(match, 0);
	}
	public static Location getRandomLocation(Match match, int yOffset)
	{
		return getRandomLocation(match.x, match.y + yOffset, match.w, match.h);
	}
	public static Location getRandomLocation(Match match, int xOffset, int yOffset)
	{
		return getRandomLocation(match.x + xOffset, match.y + yOffset, match.w, match.h);
	}
	public static Location getRandomLocation(int upperX, int upperY, int varianceX, int varianceY)
	{
		Random rand = new Random();
		return new Location(upperX + rand.nextInt(varianceX), upperY + rand.nextInt(varianceY));
	}
	
	public static int getRandomIntRange(int min, int max)
	{
		Random rand = new Random();
		return rand.nextInt(max - min) + min;
	}
	
	public static int getRandomInt(int max)
	{
		Random rand = new Random();
		return rand.nextInt(max);
	}
}
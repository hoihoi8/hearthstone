package Framework;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Logger {
	public static void log(String message)
	{
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
		Date date = new Date();
		final String outputLocation = System.getProperty("user.dir") + "\\bin\\output\\";
		final String fileName = outputLocation + "log" + dateFormat.format(date) + ".txt";
		
		System.out.println(date.toString() + ": " + message);
		
//		if(!new File(outputLocation).exists()) {
//			new File(outputLocation).mkdir();
//		}
//		else {
//			new File("C:\\Directory1").mkdir();
//		}
//		
//		try(BufferedWriter bw = new BufferedWriter(new FileWriter(fileName, true))) {
//			bw.write(message);
//		}catch (IOException e) {
//		    System.out.println("Logging to file failed");
//		}
	}
}

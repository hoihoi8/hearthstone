package FrameworkHearthStone.Screens;

import org.sikuli.script.Pattern;
import org.sikuli.script.Region;

import Framework.SikuliWrap;
import FrameworkHearthStone.Utilities.FileUtils;
import FrameworkHearthStone.Utilities.Images;

public class SeasonRewards extends BaseScreen
{
	private static final String imagesFolder = Images.seasonRewardsFolder;
	private static Pattern rootElement = FileUtils.getPattern(imagesFolder, Images.seasonRewardsNext);

	public SeasonRewards(Region gameWindow) {
		super(rootElement, "SeasonRewards", gameWindow);
	}
	
	public void handleSeasonRewards() {
		Framework.Logger.log("Handle season rewards");
		SikuliWrap.clickRandomLocation(FileUtils.getPattern(imagesFolder, Images.seasonRewardsNext), gameWindow);
		SikuliWrap.wait(gameWindow, FileUtils.getPattern(imagesFolder, Images.seasonRewardsDone));
		SikuliWrap.clickRandomLocation(FileUtils.getPattern(imagesFolder, Images.seasonRewardsDone), gameWindow);
	}
}

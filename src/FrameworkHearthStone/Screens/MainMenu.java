package FrameworkHearthStone.Screens;

import org.sikuli.script.Pattern;
import org.sikuli.script.Region;

import Framework.SikuliWrap;
import FrameworkHearthStone.Utilities.FileUtils;
import FrameworkHearthStone.Utilities.Images;

public class MainMenu extends BaseScreen
{
	private static final String imagesFolder = Images.mainMenuFolder;
	private static Pattern rootElement = FileUtils.getPattern(imagesFolder, Images.mainMenuRootElement);

	public MainMenu(Region gameWindow) {
		super(rootElement, "MainMenu", gameWindow);
	}
	
	public void clickPlay() {
		Framework.Logger.log("Click Play");
		SikuliWrap.clickRandomLocation(FileUtils.getPattern(imagesFolder, Images.mainMenuRootElement), gameWindow);
	}
	
	public void clickQuests() {
		Framework.Logger.log("clickQuests");
		SikuliWrap.clickRandomLocation(FileUtils.getPattern(imagesFolder, Images.questLogButton), gameWindow);
	}
	
	public void clickSoloAdventures() {
		Framework.Logger.log("clickSoloAdventures");
		SikuliWrap.clickRandomLocation(FileUtils.getPattern(imagesFolder, Images.soloAdventuresButton), gameWindow);
	}
}

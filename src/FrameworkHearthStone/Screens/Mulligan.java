package FrameworkHearthStone.Screens;

import java.util.Iterator;
import java.util.List;

import org.sikuli.script.FindFailed;
import org.sikuli.script.Match;
import org.sikuli.script.Pattern;
import org.sikuli.script.Region;
import org.sikuli.script.Screen;

import Framework.Logger;
import Framework.SikuliWrap;
import FrameworkHearthStone.DataStructures.CardData;
import FrameworkHearthStone.Modules.BoardIdleManager;
import FrameworkHearthStone.Utilities.FileUtils;
import FrameworkHearthStone.Utilities.Images;
import FrameworkHearthStone.Utilities.ListUtils;
import FrameworkHearthStone.Utilities.SikuliDrawUtils;
import FrameworkHearthStone.Utilities.SleepUtils;

public class Mulligan extends BaseScreen
{
	BoardIdleManager idleManager;
	private static final String imagesFolder = Images.mulliganFolder;
	private static final Pattern rootElementImage = FileUtils.getPattern(imagesFolder, Images.ConfirmButton);
	private Match confirmMatch = null;
	private Region cardRegion = null;
	private CardData[] cardsToMulligan = new CardData[4];
	private int cardCount = 0;
	
	public Mulligan(Region gameWindow) {
		super(rootElementImage, "Mulligan", gameWindow);
		
		idleManager = new BoardIdleManager(this.gameWindow);
	}
	
	public void executeMulligan()
	{
		Logger.log("Mulligan Phase");
		buildHandList();
		
		if(handContainsCardOfCost("1"))
        {
			markCardKeep("1");
            if (handContainsCardOfCost("2"))
            {
                markCardKeep("2");
            }
            else
            {
                mulliganRestOfHand();
                return;
            }

            if (handContainsCardOfCost("3"))
            {
                markCardKeep("3");
                if (handContainsCardOfCost("4"))
                {
                	markCardKeep("4");
                	mulliganRestOfHand();
                }
                else
                {
                	mulliganRestOfHand();
                    return;
                }

            }
            else
            {
                mulliganRestOfHand();
                return;
            }

        }

        else if(handContainsCardOfCost("2"))
        {
            markCardKeep("2");
            if(handContainsCardOfCost("3"))
            {
            	markCardKeep("3");
                if(handContainsCardOfCost("4"))
                {
                	markCardKeep("4");
                    mulliganRestOfHand();
                }
                else
                {
                	mulliganRestOfHand();
                	return;
                }
            }
            else
            {
            	mulliganRestOfHand();
                return;
            }
        }
        else
        {
        	mulliganRestOfHand();
        	return;
        }
    }

	private boolean handContainsCardOfCost(String cardCost) {
		boolean result = false;
		for(int i=0; i<cardCount; i++)
		{
			if(cardsToMulligan[i].manaCost.equals(cardCost))
			{
				result = true;
			}
		}
		return result;
	}

	private void markCardKeep(String cardCost)
	{
		Logger.log("markCardKeep: " + cardCost);
		for(int i=0; i<cardCount; i++)
		{
			if(cardsToMulligan[i].manaCost.equals(cardCost) && cardsToMulligan[i].keepCardInMulligan == false)
			{
				cardsToMulligan[i].keepCardInMulligan = true;
				break;
			}
		}
	}

	private void buildHandList()
	{
		Logger.log("buildHandList");
		int index = 0;
		for(int i=0; i<Images.cardCostListBig.length; i++)
		{
			try
			{
				Pattern pattern = new Pattern(FileUtils.getPattern(Images.cardsBigFolder, Images.cardCostListBig[i])).similar(.84f);
				Iterator<Match> matches = getCardRegion().findAll(pattern);
				List<Match> matchesList = ListUtils.copyIteratorToList(matches);
				for (Match match : matchesList)
				{
					if(cardsToMulligan[index] == null)
					{
						cardsToMulligan[index] = new CardData();
					}
					cardsToMulligan[index].manaCost = Images.cardCostListBig[i].substring(0, 1);
					cardsToMulligan[index].cardManaArtBigImage = FileUtils.imageName(Images.cardsBigFolder, Images.cardCostListBig[i]);
					cardsToMulligan[index].keepCardInMulligan = false;
					cardsToMulligan[index].match = match;
					Logger.log("Card Found:" + Images.cardCostListBig[i] + " " + match.getScore());
					cardCount++;
					index++;
				}
			}
			catch(FindFailed e) {}
		}
	}

	public boolean isGoingFirst(Screen screen) {
		return cardCount < 4;
	}

	private void mulliganRestOfHand()
    {
		Logger.log("mulliganRestOfHand");
		for(int i=0; i<cardCount; i++)
		{
			if(cardsToMulligan[i].keepCardInMulligan.booleanValue() == false)
			{
				try
				{
					getCardRegion().click(cardsToMulligan[i].match);
					SleepUtils.waitRandomTime(375, 400);
				}
				catch(FindFailed e) {
					Logger.log(e.getMessage());
				}
			}
		}
		
		idleManager.setMouseToIdle();
		clickConfirmButton();
		idleManager.setMouseToIdle();
		SleepUtils.waitRandomTime(4000, 4500);
		
		for(int i=0; i<90; i++)
		{
			if(SikuliWrap.exists(getCardRegion(), FileUtils.getPattern(Images.mulliganFolder, Images.waiting)) != null)
			{
				Framework.Logger.log("Waiting for opponent to mulligan");
				SleepUtils.waitRandomTime(1000, 1000);
			}
			else
			{
				break;
			}
		}
		
		SleepUtils.waitRandomTime(2000, 2500);
    }

	private void clickConfirmButton() {
		Logger.log("clickConfirmButton");
		try {
			getCardRegion().click(getConfirmButtonMatch());
			SleepUtils.waitRandomTime(375, 400);
		} catch (FindFailed e) {
			Framework.Logger.log("Unable to click on the confirm mulligan button");
		}
		
		if(cardsToMulligan.length == 3)
		{
			SleepUtils.waitRandomTime(3000, 4000);
		}
	}
	
	private Match getConfirmButtonMatch()
	{
		if(confirmMatch == null)
		{
			confirmMatch = SikuliWrap.verifyExists(getCardRegion(), FileUtils.getPattern(Images.mulliganFolder, Images.ConfirmButton));
		}
		
		return confirmMatch;
	}
	
	private Region getCardRegion()
	{
		if(cardRegion == null)
		{
			cardRegion = Region.create(gameWindow.x + 50, gameWindow.y + 200, 900, 500);
			SikuliDrawUtils.DrawRectangleAroundRegion(cardRegion);
			cardRegion.setAutoWaitTimeout(0);
		}
		
		return cardRegion;
	}
}
package FrameworkHearthStone.Screens;

import org.sikuli.script.Match;
import org.sikuli.script.Pattern;
import org.sikuli.script.Region;

import Framework.SikuliWrap;
import FrameworkHearthStone.DataStructures.Enums.classes;
import FrameworkHearthStone.Utilities.FileUtils;
import FrameworkHearthStone.Utilities.Images;
import FrameworkHearthStone.Utilities.SikuliDrawUtils;

public class QuestLog extends BaseScreen
{
	private static final String imagesFolder = Images.questLogFolder;
	private static Pattern rootElement = FileUtils.getPattern(imagesFolder, Images.questLogRootElement);

	public QuestLog(Region gameWindow) {
		super(rootElement, "QuestLog", gameWindow);
	}
	
	public void clickExit()
	{
		SikuliWrap.clickRandomLocation(FileUtils.getPattern(imagesFolder, Images.questLogExit), gameWindow);
	}
	
	public void ditchGarbageQuests()
	{
		for(String imageName : Images.ditchQuestList)
		{
			Match match = SikuliWrap.exists(gameWindow, FileUtils.getPattern(imagesFolder, imageName, .96f));
			if(match != null)
			{
				Framework.Logger.log("Try to ditch Quest: " + imageName);
				Region quitQuestRegion = Region.create(match.x + match.w - 28, match.y - 140, 48, 90);
				SikuliDrawUtils.DrawRectangleAroundRegion(quitQuestRegion);
				SikuliWrap.clickRandomLocation(FileUtils.getPattern(imagesFolder, Images.quitQuest), quitQuestRegion);
			}
		}
	}

	public classes getClassToPlay() {
		int hunter = 0;
		int paladin = 0;
		int rogue = 0;
		int warlock = 0;
		int druid = 0;
		int mage = 0;
		int shaman = 0;
		int warrior = 0;
		int priest = 0;
		
		int highestClassPoints = hunter;
		classes classToPlay = classes.hunter; //Hunter Preferred
		
		for(String imageName : Images.questList)
		{
			if(SikuliWrap.exists(gameWindow, FileUtils.getPattern(imagesFolder, imageName, .92f)) != null)
			{
				if(imageName.toLowerCase().contains("hunter")) {
					hunter++;
				}
				if(imageName.toLowerCase().contains("paladin")) {
					paladin++;
				}
				if(imageName.toLowerCase().contains("rogue")) {
					rogue++;
				}
				if(imageName.toLowerCase().contains("warlock")) {
					warlock++;
				}
				if(imageName.toLowerCase().contains("druid")) {
					druid++;
				}
				if(imageName.toLowerCase().contains("mage")) {
					mage++;
				}
				if(imageName.toLowerCase().contains("shaman")) {
					shaman++;
				}
				if(imageName.toLowerCase().contains("warrior"))
				{
					warrior++;
				}
				if(imageName.toLowerCase().contains("priest"))
				{
					priest++;
				}
				if(imageName.toLowerCase().contains("cast40Spells")) {
					hunter++;
				}
				if(imageName.toLowerCase().contains("Deal100DamageToEnemyHeroes")) {
					hunter++;
				}
				if(imageName.toLowerCase().contains("Destroy40Minions")) {
					hunter++;
				}
				if(imageName.toLowerCase().contains("Win3GamesWithAnyClass")) {
					hunter++;
				}
				if(imageName.toLowerCase().contains("win7GamesInAnyMode")) {
					hunter++;
				}
			}
			
			highestClassPoints = hunter; //hunter preferred
			
			if(paladin > highestClassPoints) {
				highestClassPoints = paladin;
				classToPlay = classes.paladin;
			}
			if(rogue > highestClassPoints) {
				highestClassPoints = rogue;
				classToPlay = classes.rogue;
			}
			if(warlock > highestClassPoints) {
				highestClassPoints = warlock;
				classToPlay = classes.warlock;
			}
			if(druid > highestClassPoints) {
				highestClassPoints = druid;
				classToPlay = classes.druid;
			}
			if(mage > highestClassPoints) {
				highestClassPoints = mage;
				classToPlay = classes.mage;
			}
			if(shaman > highestClassPoints) {
				highestClassPoints = shaman;
				classToPlay = classes.shaman;
			}
			if(warrior > highestClassPoints) {
				highestClassPoints = warrior;
				classToPlay = classes.warrior;
			}
			if(priest > highestClassPoints) {
				highestClassPoints = priest;
				classToPlay = classes.priest;
			}
		}
		
		Framework.Logger.log("Class chosen to play: " + classToPlay.toString() + " Score: " + Integer.toString(highestClassPoints));
		return classToPlay;
	}
}
package FrameworkHearthStone.Utilities;

import org.sikuli.script.Location;
import org.sikuli.script.Match;
import org.sikuli.script.Region;

public class SikuliDrawUtils {
	private static boolean turnOffDebugDrawing = false;
	
	public static void DrawRectangleAroundRegion(Match match)
	{
		DrawRectangleAroundRegion(Region.create(match.x, match.y, match.w, match.h), 1);
	}
	public static void DrawRectangleAroundRegion(Match match, int seconds)
	{
		DrawRectangleAroundRegion(Region.create(match.x, match.y, match.w, match.h), seconds);
	}
	public static void DrawRectangleAroundRegion(int x, int y, int w, int h, int seconds)
	{
		DrawRectangleAroundRegion(Region.create(x, y, w, h), seconds);
	}
	public static void DrawRectangleAroundRegion(Region drawRegion)
	{
		DrawRectangleAroundRegion(drawRegion, 1);
	}
	public static void DrawRectangleAroundRegion(Region drawRegion, int seconds)
	{
		if(!turnOffDebugDrawing)
		{
			Region temp = drawRegion;
			temp.highlight(seconds);
		}
	}
	
	public static void DrawRectangleAtPoint(Location location)
	{
		DrawRectangleAtPoint(location, 2);
	}
	
	public static void DrawRectangleAtPoint(Location location, int seconds)
	{
		if(!turnOffDebugDrawing)
		{
			Region temp = Region.create(location.x, location.y, 2, 2);
			temp.highlight(seconds);
		}
	}
}

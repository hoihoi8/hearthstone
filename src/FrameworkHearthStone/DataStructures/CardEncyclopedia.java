package FrameworkHearthStone.DataStructures;

import java.io.File;
import java.io.FileReader;
import java.util.HashMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import Framework.Logger;
import FrameworkHearthStone.Utilities.FileUtils;
import FrameworkHearthStone.Utilities.Images;
import FrameworkHearthStone.Utilities.ClassMapper;

public class CardEncyclopedia {
	public HashMap<String, CardData> cardDictionary = new HashMap<String, CardData>();
	private String cardConfigName = System.getProperty("user.dir") + "\\Config\\cardConfig.txt";
	
	public CardEncyclopedia()
	{
		loadCardData();
	}
	
	public void loadCardData()
	{ 
		Framework.Logger.log("loadCardData");
        try
        {
        	JSONParser parser = new JSONParser();
            JSONObject jsonObject = (JSONObject)parser.parse(new FileReader(cardConfigName));
            JSONArray cardsArray = (JSONArray) jsonObject.get("Cards");
            for (int i = 0; i < cardsArray.size(); i++) {
                JSONObject jsonCard = (JSONObject)cardsArray.get(i);
                CardData loadedCard = copyCardDataFromConfig(jsonCard);
                cardDictionary.put(loadedCard.getName(), loadedCard);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
	}

	private CardData copyCardDataFromConfig(JSONObject jsonObject)
	{
		CardData card = new CardData();
		
		if(jsonObject.containsKey("cardName"))
		{
			card.setName(jsonObject.get("cardName").toString());
			
			card.cardArtBigImage = card.getName() + ".png";
			if(!new File(FileUtils.imageName(Images.cardsBigFolder, card.cardArtBigImage)).exists())
			{
				Logger.log("fileNameBuilder:" + card.cardArtBigImage + " does not exist.");
			}
		}
		if(jsonObject.containsKey("class"))
		{
			card.cardClass = ClassMapper.getClassFromString((String)jsonObject.get("class"));
		}
		Framework.Validate.isTrue("Name can't be empty", !card.getName().equals("") && !card.getNameWithGuid().equals(""));
		
		if(jsonObject.containsKey("manaCost")) {
        	card.manaCost = (String)jsonObject.get("manaCost");
        }
		if(jsonObject.containsKey("playOrder")) {
        	card.playOrder = (String)jsonObject.get("playOrder");
        }
		if(jsonObject.containsKey("type")) {
        	card.type = (String)jsonObject.get("type");
        	
        	if(!card.type.equals("spell"))
        	{
	        	card.cardArtBoardImage = card.getName() + ".png";
	        	//TODO add flag to cardConfig for cards that aren't in hand
				//Framework.Validate.isTrue("Card board art is missing: " + card.getName(), new File(FileUtils.imageName(Images.cardsBigFolder, card.cardArtBigImage)).exists());
        	}
        }
		if(jsonObject.containsKey("subType")) {
        	card.subtype = (String)jsonObject.get("subType");
        }
		if(jsonObject.containsKey("ability")) {
        	JSONArray abilityArray = (JSONArray) jsonObject.get("ability");
        	for (int i = 0; i < abilityArray.size(); i++) {
        		card.ability.add(abilityArray.get(i).toString());
        	}
        }
		if(jsonObject.containsKey("aiPoints")) {
        	card.aiPoints = Integer.parseInt((String)jsonObject.get("aiPoints"));
        }
		if(jsonObject.containsKey("battleCry")) {
			JSONArray battlecryArray = (JSONArray) jsonObject.get("battleCry");
			for (int i = 0; i < battlecryArray.size(); i++) {
				JSONObject jsonBattleCry = (JSONObject)battlecryArray.get(i);
				
				if(jsonBattleCry.containsKey("target")) {
		        	card.battlecry.target = jsonBattleCry.get("target").toString();
		        }
				if(jsonBattleCry.containsKey("subTypeTarget")) {
		        	card.battlecry.targetSubType = jsonBattleCry.get("subTypeTarget").toString();
		        }
				if(jsonBattleCry.containsKey("damage")) {
		        	card.battlecry.damage = jsonBattleCry.get("damage").toString();
		        }
				if(jsonBattleCry.containsKey("aiPoints")) {
		        	card.battlecry.aiPoints = Integer.parseInt(jsonBattleCry.get("aiPoints").toString());
		        }
				if(jsonBattleCry.containsKey("affinityType")) {
		        	card.battlecry.affinityType = jsonBattleCry.get("affinityType").toString();
		        }
				if(jsonBattleCry.containsKey("aiAffinityPoints")) {
		        	card.battlecry.aiAffinityPoints = Integer.parseInt(jsonBattleCry.get("aiAffinityPoints").toString());
		        }
			}
        }
		
		return card;
	}
}

package FrameworkHearthStone.Modules;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.sikuli.script.FindFailed;
import org.sikuli.script.Location;
import org.sikuli.script.Pattern;
import org.sikuli.script.Region;

import Framework.SikuliWrap;
import FrameworkHearthStone.DataStructures.CardData;
import FrameworkHearthStone.DataStructures.GameData;
import FrameworkHearthStone.Utilities.FileUtils;
import FrameworkHearthStone.Utilities.Images;
import FrameworkHearthStone.Utilities.ListUtils;
import FrameworkHearthStone.Utilities.RandomUtils;
import FrameworkHearthStone.Utilities.ScreenshotUtils;
import FrameworkHearthStone.Utilities.SleepUtils;

public class AI {
	private BoardIdleManager boardIdleManager = null;
	private Region gameWindow;

	public AI(Region gameWindow) {
		this.gameWindow = gameWindow;
	}

	private BoardIdleManager boardIdleManager() {
		if (boardIdleManager == null) {
			boardIdleManager = new BoardIdleManager(gameWindow);
		}
		return boardIdleManager;
	}

	public List<CardData> chooseCombinationToPlay(GameData gameData) {
		Framework.Logger.log("chooseCombinationToPlay");
		// TODO make better decisions
		List<CardData> cardsToPlay = new ArrayList<CardData>(
				getCardCombinationWithHighestScore(gameData));

		Framework.Logger.log("Best combination:");
		ListUtils.printCardNamesFromCardDataList(cardsToPlay, "Card to Play:");
		return cardsToPlay;
	}

	private List<CardData> getCardCombinationWithHighestScore(GameData gameData) {
		Set<CardData> tempList = new HashSet<CardData>(
				gameData.cardsInHand.values());
		List<CardData> combinationsToPlay = new ArrayList<CardData>();
		Set<Set<CardData>> cardCombinations = this.GetPowerSet(tempList);

		if (cardCombinations != null) {
			int highestPointsTotal = 0;
			boolean hasCoin = isCoinInHashMap(gameData.cardsInHand);
			for (Set<CardData> cardSet : cardCombinations) {
				if (cardSet.size() <= 0) {
					continue;
				}

				int totalMana = 0;
				int weaponCount = 0;
				for (CardData card : cardSet) {
					if (card.type.equals("weapon")) {
						weaponCount++;
					}
				}
				if (weaponCount > 1) {
					continue;
				}

				boolean secretAlreadyInPlay = false;
				for (CardData card : cardSet) {
					for (CardData cardOnBoard : gameData.cardsOnBoard.values()) {
						if (cardOnBoard.type.equals("secret")
								&& card.getName().equals(cardOnBoard.getName())) {
							Framework.Logger
									.log("Secret already found on board");
							secretAlreadyInPlay = true;
						}
					}
					totalMana += Integer.parseInt(card.manaCost);
				}

				int manaModifier = hasCoin ? 1 : 0;
				if (totalMana > gameData.mana + manaModifier
						|| secretAlreadyInPlay) {
					continue;
				}

				int totalPoints = 0;
				totalPoints += calculateBaseAiPoints(cardSet);
				totalPoints += calculateBattleCryAiPoints(cardSet, gameData);

				if (totalMana > gameData.mana) {
					if (!isCoinInSet(cardSet)) {
						cardSet = addCoinToSet(gameData, cardSet);
						totalPoints -= 1;
					}
				}

				if (totalPoints > highestPointsTotal) {
					combinationsToPlay = new ArrayList<CardData>(cardSet);
					highestPointsTotal = totalPoints;
				}
			}
		}
		return combinationsToPlay;
	}

	private Set<CardData> addCoinToSet(GameData gameData, Set<CardData> cardSet) {
		boolean coinAdded = false;
		for (CardData card : gameData.cardsInHand.values()) {
			if (card.getName().startsWith("coin")) {
				cardSet.add(card);
				coinAdded = true;
			}
		}
		Framework.Validate.isTrue("Failed trying to add coin to CardsToPlay",
				coinAdded);
		return cardSet;
	}

	public List<CardData> removeCoin(List<CardData> cardList) {
		Framework.Logger.log("Removing coin from hand");
		List<CardData> tempSet = new ArrayList<CardData>(cardList);
		for (CardData card : tempSet) {
			if (card.getName().startsWith("coin")) {
				cardList.remove(card);
			}
		}

		return cardList;
	}

	public void playCoin(GameData gameData) {
		Framework.Logger.log("playCoin");
		for (CardData card : gameData.cardsInHand.values()) {
			if (card.getName().startsWith("coin")) {
				playCard(card, gameData);
				break;
			}
		}
	}

	public boolean isCoinInHashMap(HashMap<String, CardData> cardsInHand) {
		boolean containsCoin = false;
		for (String name : cardsInHand.keySet()) {
			if (name.startsWith("coin")) {
				containsCoin = true;
			}
		}
		return containsCoin;
	}

	private boolean isCoinInSet(Set<CardData> cardSet) {
		boolean containsCoin = false;
		for (CardData card : cardSet) {
			if (card.getName().startsWith("coin")) {
				containsCoin = true;
			}
		}
		return containsCoin;
	}

	public boolean isCoinInList(List<CardData> cardsList) {
		boolean containsCoin = false;
		for (CardData name : cardsList) {
			if (name.getName().startsWith("coin")) {
				containsCoin = true;
			}
		}
		return containsCoin;
	}

	private int calculateBaseAiPoints(Set<CardData> cardList) {
		int basePoints = 0;
		for (CardData card : cardList) {
			basePoints += card.aiPoints;
		}
		return basePoints;
	}

	private int calculateBattleCryAiPoints(Set<CardData> cardList, GameData gameData) {
		int battleCryPoints = 0;
		for (CardData card : cardList) {
			if (card.battlecry != null && card.battlecry.target != null
					&& card.battlecry.target.equals("enemy")) {
				if (card.battlecry != null
						&& card.battlecry.affinityType != null) {
					for (CardData cardOnBoard : gameData.cardsOnBoard.values()) {
						if (cardOnBoard.subtype
								.equals(card.battlecry.affinityType)) {
							Framework.Logger
									.log("Battlecry Affinity aiPoints added:"
											+ Integer
													.toString(card.battlecry.aiPoints));
							battleCryPoints += card.battlecry.aiPoints;
							break;
						}
					}
				}
				else {
					battleCryPoints += card.battlecry.aiPoints;
				}
			}
			else if (card.battlecry != null && card.battlecry.target != null && card.battlecry.target.equals("self"))
			{
				battleCryPoints += card.battlecry.aiPoints;
			}
			else if (card.battlecry != null && card.battlecry.target != null && card.battlecry.target.equals("enemyTaunt"))
			{
				if(gameData.opponentsTaunts.values().size() > 0) {
					Framework.Logger.log("Battlecry EnemyTaunt target aiPoints added:" + Integer.toString(card.battlecry.aiPoints));
					battleCryPoints += card.battlecry.aiPoints;
				}
			} else if (card.battlecry != null && card.battlecry.target != null
					&& card.battlecry.target.equals("existingWeapon")) {
				for (CardData isCardWeapon : gameData.cardsOnBoard.values()) {
					if (isCardWeapon.type.equals("weapon")) {
						battleCryPoints += card.battlecry.aiPoints;
						Framework.Logger
								.log("Battlecry existing weapon aiPoints subracted:"
										+ Integer
												.toString(card.battlecry.aiPoints));
						break;
					}
				}
			}
		}

		return battleCryPoints;
	}

	public CardData playCard(CardData card, GameData gameData) {
		Framework.Logger.log("playCard: " + card.getName());
		return moveCardFromHandToPlay(card, gameData);
	}

	public boolean attackCard(CardData card, GameData gameData) {
		Framework.Logger.log("attackCard :" + card.getName());
		boolean hasAttacked = false;
		if(card.canAttack)
		{
			hasAttacked = executeAttack(card, gameData, hasAttacked);
		}

		return hasAttacked;
	}

	public boolean attackWeapon(CardData card, GameData gameData) {
		Framework.Logger.log("attackWeapon: " + card.getName());
		boolean hasAttacked = false;
		if (card.getName().equals("weapon")) {
			hasAttacked = executeAttack(card, gameData, hasAttacked);
		}

		return hasAttacked;
	}

	private boolean executeAttack(CardData card, GameData gameData, boolean hasAttacked)
	{
			SikuliWrap.clickRandomLocation(card.match, gameData.playerBoardRegion);
			SleepUtils.waitRandomTime(425, 450);

			if (gameData.opponentsTaunts.values().size() == 0) {
				SikuliWrap.clickRandomLocation(gameData.opponentFace, gameData.opponentFaceRegion);
				SleepUtils.waitRandomTime(375, 400);

				Pattern pattern = FileUtils.getPattern(Images.boardFolder, Images.tauntText).similar(.95f);
				if (SikuliWrap.exists(gameData.tauntTextRegion, pattern) != null) {
					SleepUtils.waitRandomTime(2700, 2700);
					ScreenshotUtils.takeScreenshot("tauntNotFound");
					
					for (CardData taunt : gameData.opponentsTauntsBackup.values()) {
						SikuliWrap.clickLocation(RandomUtils.getRandomLocation(taunt.match, 12, 12), gameData.opponentBoardRegion);
						SleepUtils.waitRandomTime(375, 400);
						hasAttacked = true;
						break;
					}

					boardIdleManager().setMouseToIdle();
				}
				else {
					hasAttacked = true;
				}

				boardIdleManager().setMouseToIdle();
			} else {
				for (CardData taunt : gameData.opponentsTaunts.values()) {
					if(SikuliWrap.clickLocation(RandomUtils.getRandomLocation(taunt.match, -taunt.match.h - 8), gameData.opponentBoardRegion))
					{
						SleepUtils.waitRandomTime(375, 400);
						hasAttacked = true;
						break;
					}
				}

				boardIdleManager().setMouseToIdle();
			}

			SleepUtils.waitRandomTime(375, 400);
		return hasAttacked;
	}

	private CardData moveCardFromHandToPlay(CardData card, GameData gameData) {
		Framework.Logger.log("moveCardFromHandToPlay");

		clickOnCardInHand(gameData, card);
		clickOnBoardTarget(gameData, card);

		return card;
	}

	private void clickOnCardInHand(GameData gameData, CardData cardToPlay)
	{
		try {
			Framework.Validate .isTrue("card: " + cardToPlay.getName() + " does not have match data", cardToPlay.getName() != null);
			cardToPlay.match.x += 10;
			gameData.handRegion.click(RandomUtils
					.getRandomLocation(cardToPlay.match));
			SleepUtils.waitRandomTime(375, 400);

			if (cardToPlay.getName().contains("heroPower")) {
				SleepUtils.waitRandomTime(375, 400);	//Hero power seems touchy. Extra wait
				boardIdleManager().setMouseToIdle(false);
			}
		} catch (FindFailed e) {
			Framework.Logger.log("Could not click on hard in hand: "
					+ cardToPlay.getNameWithGuid());
		}
	}

	private void clickOnBoardTarget(GameData gameData, CardData cardToPlay) {
		if (cardToPlay.getName().contains("heroPower")) {
			return;
		}
		try {
			if (cardToPlay.type.equals("spell")) {
				gameData.opponentFaceRegion.click(RandomUtils.getRandomLocation(gameData.opponentFace));
				SleepUtils.waitRandomTime(375, 400);
				boardIdleManager().setMouseToIdle();
			} else {
				executeBoardClick(gameData.playerBoardRegion);
			}

		} catch (FindFailed e) {
			Framework.Logger.log("Could not play card from hand: " + cardToPlay.getNameWithGuid());
		}
	}

	private void executeBoardClick(Region clickRegion) throws FindFailed {
		Location boardClickLocation = RandomUtils.getRandomLocation(clickRegion);
		clickRegion.click(boardClickLocation);
		SleepUtils.waitRandomTime(375, 400);
		boardIdleManager().setMouseToIdle(false);
	}

	public boolean playCardBattleCryTarget(GameData gameData, CardData cardToPlay)
	{
		Framework.Logger.log("playCardBattleCryTarget");
		boolean getNewOppBoardStates = false;
		if (cardToPlay.type.equals("spell")) {
			boardIdleManager().setMouseToIdle();
			return getNewOppBoardStates;
		}

		try
		{
			if (cardToPlay.battlecry != null
					&& cardToPlay.battlecry.target != null
					&& cardToPlay.battlecry.target.equals("enemy")) {
				gameData.opponentFaceRegion.click(RandomUtils.getRandomLocation(gameData.opponentFace));
				SleepUtils.waitRandomTime(375, 400);
				boardIdleManager().setMouseToIdle();
			}
			if (cardToPlay.battlecry != null
					&& cardToPlay.battlecry.target != null
					&& cardToPlay.battlecry.target.equals("self")) {
				gameData.playerPortraitRegion.click(RandomUtils.getRandomLocation(gameData.playerPortraitRegion));
				SleepUtils.waitRandomTime(375, 400);
				boardIdleManager().setMouseToIdle();
			} else if (cardToPlay.battlecry != null
					&& cardToPlay.battlecry.target != null
					&& cardToPlay.battlecry.target.equals("enemyTaunt")) {
				boolean tauntClicked = false;
				for (CardData taunt : gameData.opponentsTaunts.values()) {
					gameData.opponentBoardRegion.click(RandomUtils.getRandomLocation(taunt.match,-taunt.match.h - 8));
					SleepUtils.waitRandomTime(375, 400);
					boardIdleManager().setMouseToIdle();
					getNewOppBoardStates = true;
					tauntClicked = true;
					break;
				}

				if (!tauntClicked) {
					boardIdleManager().setMouseToIdle();
				}
			} else {
				boardIdleManager().setMouseToIdle();
			}
		} catch (FindFailed e) {
			Framework.Logger
					.log("Could not play on card's battlecry target from hand: "
							+ cardToPlay.getNameWithGuid());
			boardIdleManager().setMouseToIdle();
		}

		return getNewOppBoardStates;
	}

	public <T> Set<Set<T>> GetPowerSet(Set<T> originalSet) {
		Set<Set<T>> sets = new HashSet<Set<T>>();
		if (originalSet.isEmpty()) {
			sets.add(new HashSet<T>());
			return sets;
		}
		List<T> list = new ArrayList<T>(originalSet);
		T head = list.get(0);
		Set<T> rest = new HashSet<T>(list.subList(1, list.size()));
		for (Set<T> set : GetPowerSet(rest)) {
			Set<T> newSet = new HashSet<T>();
			newSet.add(head);
			newSet.addAll(set);
			sets.add(newSet);
			sets.add(set);
		}
		return sets;
	}
}

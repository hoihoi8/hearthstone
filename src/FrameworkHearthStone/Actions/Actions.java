package FrameworkHearthStone.Actions;

import Tests.BaseTest;

public class Actions extends BaseTest {
	NavigationActions navigationActions;
	
	public NavigationActions navigationActions()
	{
		if(navigationActions == null)
		{
			navigationActions = new NavigationActions();
		}
		
		return navigationActions;
	}
}

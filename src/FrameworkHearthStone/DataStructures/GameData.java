package FrameworkHearthStone.DataStructures;

import java.util.HashMap;
import java.util.List;

import org.sikuli.script.Match;
import org.sikuli.script.Region;

import FrameworkHearthStone.DataStructures.Enums.classes;

public class GameData {
	public HashMap<String, CardData> cardsInHand;
	public HashMap<String, CardData> cardsOnBoard;
	public HashMap<String, CardData> opponentsTaunts;
	public HashMap<String, CardData> opponentsTauntsBackup;
	public List<CardData> cardsToPlayThisTurn;
	
	public int mana;
	public classes playerClass;
	public classes oppponentClass;
	public boolean coinUsedPlayer;
	public boolean coinUsedOpponent;
	public boolean playerWentFirst;
	
	public Region handRegion;
	public Region playerBoardRegion;
	public Region battleCryPlayRegion;
	public Region opponentBoardRegion;
	public Region opponentFaceRegion;
	public Region playerHandRegion;
	public Match opponentFace;
	public Match heroPowerMatch;
	public Region playerWeaponRegion;
	public Region opponentsTauntRegion;
	public Region tauntTextRegion;
	public Region playerSecretRegion;
	public Region playerPortraitRegion;
}

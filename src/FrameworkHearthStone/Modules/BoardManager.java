package FrameworkHearthStone.Modules;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.sikuli.script.FindFailed;
import org.sikuli.script.Match;
import org.sikuli.script.Pattern;
import org.sikuli.script.Region;

import Framework.SikuliWrap;
import FrameworkHearthStone.DataStructures.CardData;
import FrameworkHearthStone.DataStructures.CardEncyclopedia;
import FrameworkHearthStone.DataStructures.GameData;
import FrameworkHearthStone.Utilities.FileUtils;
import FrameworkHearthStone.Utilities.Images;
import FrameworkHearthStone.Utilities.ListUtils;
import FrameworkHearthStone.Utilities.SikuliDrawUtils;

public class BoardManager {
	
	private CardEncyclopedia cardEncyclopedia;
	private String imageFolder = Images.boardFolder;
	
	public BoardManager(CardEncyclopedia cardEncyclopedpia)
	{
		this.cardEncyclopedia = cardEncyclopedpia;
	}
	
	public HashMap<String, CardData> getPlayerBoardState(GameData gameData, boolean getSecrets)
	{
		Framework.Logger.log("getPlayerBoardState");
		HashMap<String, CardData> CardsOnBoard = new HashMap<String, CardData>();
		CardsOnBoard = findWeaponsOnBoard(gameData, CardsOnBoard);
		if(getSecrets)
		{
			CardsOnBoard = findSecretsOnBoard(gameData, CardsOnBoard);
		}
		CardsOnBoard = setCanCardsOnBoardAttack(gameData, CardsOnBoard);
		
		return CardsOnBoard;
	}

	private HashMap<String, CardData> findSecretsOnBoard(GameData gameData, HashMap<String, CardData> cardsOnBoard) {
		if(SikuliWrap.exists(gameData.playerWeaponRegion, FileUtils.getPattern(Images.boardFolder, Images.secret)) != null)
		{
			try {
				Iterator<Match> matches = gameData.playerWeaponRegion.findAll(FileUtils.getPattern(Images.boardFolder, Images.secret));
				List<Match> matchesList = ListUtils.copyIteratorToList(matches);
				for(Match secret : matchesList)
				{
					gameData.playerWeaponRegion.hover(secret);
					gameData.cardsOnBoard = findKnownCardsInRegion(gameData, cardsOnBoard, gameData.playerSecretRegion, "secret");
				}
			} catch (FindFailed e) {
				Framework.Validate.fail("Could not find the secret");
			}
		}
		return cardsOnBoard;
	}

	private HashMap<String, CardData> findWeaponsOnBoard(GameData gameData, HashMap<String, CardData> cardsOnBoard)
	{
		return findKnownCardsInRegion(gameData, cardsOnBoard, gameData.playerWeaponRegion, "weapon");
	}

	private HashMap<String, CardData> findKnownCardsInRegion(GameData gameData, HashMap<String, CardData> CardsOnBoard, 
			Region searchRegion, String targetType) {
		for(CardData card : cardEncyclopedia.cardDictionary.values())
		{
			if(card.type.equals(targetType))
			{
				try {
					Iterator<Match> matches = searchRegion.findAll(FileUtils.getPattern(Images.cardsBoardFolder, card.cardArtBoardImage));
					List<Match> matchesList = ListUtils.copyIteratorToList(matches);
					for(Match match : matchesList)
					{
						Framework.Logger.log("Card found:" + card.getName());
						card.match = match;
						CardsOnBoard.put(card.getNameWithGuid(), card);
						SikuliDrawUtils.DrawRectangleAroundRegion(card.match);
					}
				} catch (FindFailed e) {
					if(e.getMessage().contains("Image not valid"))
					{
						Framework.Logger.log("Image not valid:" + card.getName());
					}
				}
			}
		}
		
		return CardsOnBoard;
	}
	
	private HashMap<String, CardData> setCanCardsOnBoardAttack(GameData gameData, HashMap<String, CardData> CardsOnBoard)
	{
		List<String> attackPatterns = Arrays.asList(Images.cardAttackUniversal, Images.cardAttackUniversalTaunt,
				Images.cardAttackSilenced);
		
		CardsOnBoard = setAllCardsToNoAttack(CardsOnBoard);
		for(String pattern : attackPatterns)
		{
			
			CardsOnBoard = findAttackPattern(gameData, CardsOnBoard, pattern);
		}
		
		Iterator<Match> heroWeaponMatch = null;
		try {
			Pattern pattern = new Pattern(FileUtils.getPattern(Images.cardsBoardFolder, Images.heroWeaponAttack)).similar(.80f);
			heroWeaponMatch = gameData.playerWeaponRegion.findAll(pattern);	
		} catch (FindFailed e) { }
		CardsOnBoard = setHeroToAttack(CardsOnBoard, heroWeaponMatch);
		
		return CardsOnBoard;
	}

	private HashMap<String, CardData> findAttackPattern(GameData gameData, HashMap<String, CardData> CardsOnBoard, String patternName) {
		Iterator<Match> matches = null;
		try {
			Pattern pattern = FileUtils.getPattern(Images.cardsBoardFolder, patternName).similar(.77f);
			matches = gameData.playerBoardRegion.findAll(pattern);			 
		}
		catch(Exception e) {}
		CardsOnBoard = setCardDataToAttack(CardsOnBoard, matches);
		return CardsOnBoard;
	}

	private HashMap<String, CardData> setAllCardsToNoAttack(HashMap<String, CardData> cardsOnBoard) {
		for(CardData card : cardsOnBoard.values())
		{
			card.canAttack = false;
		}
		return cardsOnBoard;
	}

	private HashMap<String, CardData> setHeroToAttack(HashMap<String, CardData> cardsOnBoard, Iterator<Match> heroWeaponMatch)
	{
		List<Match> matchesList = ListUtils.copyIteratorToList(heroWeaponMatch);
		for(Match match : matchesList)
		{
			match.x += 40;	//image contains pixels outside the portrait
			match.w += 25;
			Framework.Logger.log("Card can attack: Hero Weapon");
			cardsOnBoard = addWeaponAttack(cardsOnBoard, match, true);
		}
		
		return cardsOnBoard;
	}

	private HashMap<String, CardData> setCardDataToAttack(HashMap<String, CardData> cardsOnBoard, Iterator<Match> attackMatches) {
		List<Match> attackMatchesList = ListUtils.copyIteratorToList(attackMatches);
		List<Match> attackMatchesFound = new ArrayList<Match>();
		for(CardData card : cardsOnBoard.values())
		{
			for(Match match : attackMatchesList)
			{
				if(Math.abs(match.x - card.match.x) < 45 && Math.abs(match.y - card.match.y) < 50)
				{
					Framework.Logger.log("Card can attack: " + card.getName());
					attackMatchesFound.add(match);
					card.canAttack = true;
					break;
				}
			}
		}
		
		for(Match match : attackMatchesList)
		{
			boolean matchFound = false;
			for(Match foundMatch : attackMatchesFound)
			{
				if(foundMatch.x == match.x && foundMatch.y == match.y)
				{
					matchFound = true;
					break;
				}
			}
			
			if(!matchFound)
			{
				match.x = match.x + 20;	//image contains pixels outside card
				Framework.Logger.log("Card can attack: Unknown");
				cardsOnBoard = addUnknownCard(cardsOnBoard, match, true);
			}
		}
		
		return cardsOnBoard;
	}
	
	public int countCardsThatCanAttack(HashMap<String, CardData> cardsOnBoard)
	{
		int cardsThatCanAttack = 0;
		for(CardData card : cardsOnBoard.values())
		{
			if(card.canAttack)
			{
				cardsThatCanAttack++;
			}
		}
		
		return cardsThatCanAttack;
	}
	
	public GameData getOpponentsBoardState(GameData gameData)
	{
		Framework.Logger.log("getOpponentsBoardState");
		
		List<String> tauntImages = Arrays.asList(Images.tauntCreature, Images.tauntCreatureWithDivineShield, Images.deathrattleTaunt,
				Images.deathrattleTauntDivineShield, Images.triggerTaunt, Images.triggerTauntDivineShield, Images.tauntCreatureWithZ1,
				Images.tauntCreatureWithZ2, Images.tauntWithGreen, Images.goldenTaunt, Images.goldenTauntDivineShield, Images.malganistaunt,
				Images.tauntInspire, Images.tauntInspireDivineShield, Images.tauntFrozen);
		gameData.opponentsTaunts = processFoundTaunts(gameData, tauntImages, 0, 40);
		
		List<String> backupTauntImages = Arrays.asList(Images.tauntDivineShieldUpperLeft, Images.tauntUpperLeft, Images.goldentauntdivineshieldUpperLeft);
		gameData.opponentsTauntsBackup = processFoundTaunts(gameData, backupTauntImages, 40, -20);
		
		return gameData;
	}

	private HashMap<String, CardData> processFoundTaunts(GameData gameData, List<String> tauntImages, int xOffset, int yOffset)
	{
		HashMap<String, CardData> tauntTargets = new HashMap<String, CardData>();
		
		for(String tauntImage : tauntImages)
		{
			try {
				Pattern pattern = FileUtils.getPattern(imageFolder, tauntImage).similar(.77f);
				List<Match> tauntCreatures = ListUtils.copyIteratorToList(gameData.opponentBoardRegion.findAll(pattern));
				tauntTargets = addTauntsToOpponentsBoard(tauntTargets, createTauntCardData(tauntCreatures, false), Images.tauntCreature, xOffset, yOffset);
			} catch (FindFailed e) {}
		}
		return tauntTargets;
	}

	private HashMap<String, CardData> addTauntsToOpponentsBoard(HashMap<String, CardData> tauntTargets, HashMap<String, CardData> tauntsToAdd, 
			String fileName, int xOffset, int yOffset)
	{
		boolean tauntAlreadyFound = false;
		for(CardData taunt : tauntsToAdd.values())
		{
			for(CardData existingTaunt : tauntTargets.values())
			{
				if(Math.abs(taunt.match.x - existingTaunt.match.x) < 20)
				{
					tauntAlreadyFound = true;
					break;
				}
			}
			
			if(!tauntAlreadyFound)
			{
				Framework.Logger.log("New Taunt creature found on opponents board: X:" 
						+ Integer.toString(taunt.match.x) + " Y:" + Integer.toString(taunt.match.y) + " Score:"
						+ Double.toString(taunt.match.getScore()));
				SikuliDrawUtils.DrawRectangleAroundRegion(taunt.match);
				taunt.match.y -= 40;
				tauntTargets.put(taunt.getNameWithGuid(), taunt);
			}
		}
		return tauntTargets;
	}

	private HashMap<String, CardData> createTauntCardData(List<Match> tauntCreatures, boolean divineShield) {
		HashMap<String, CardData> tauntCardData = new HashMap<String, CardData>();
		for(Match match : tauntCreatures)
		{
			CardData cardData = new CardData();
			cardData.setName("tauntCreature");
			cardData.match = match;
			cardData.ability.add("taunt");
			if(divineShield) {
				cardData.ability.add("divineShield");
			}
			tauntCardData.put(cardData.getNameWithGuid(), cardData);
		}
		
		return tauntCardData;
	}

	private HashMap<String, CardData> addUnknownCard(HashMap<String, CardData> attackingCardsOnBoard, Match match, boolean canAttack) {
		CardData unknownCard = new CardData();
		unknownCard.setName("unknown");
		unknownCard.match = match;
		unknownCard.canAttack = canAttack;
		attackingCardsOnBoard.put(unknownCard.getName(), unknownCard);
		return attackingCardsOnBoard;
	}
	
	private HashMap<String, CardData> addWeaponAttack(HashMap<String, CardData> attackingCardsOnBoard, Match match, boolean canAttack) {
		CardData unknownCard = new CardData();
		unknownCard.setName("weapon");
		unknownCard.match = match;
		unknownCard.canAttack = canAttack;
		attackingCardsOnBoard.put(unknownCard.getName(), unknownCard);
		return attackingCardsOnBoard;
	}
}

package FrameworkHearthStone.Screens;

import org.sikuli.script.Pattern;
import org.sikuli.script.Region;

import Framework.SikuliWrap;
import FrameworkHearthStone.HearthstoneEnums.adventureDifficulty;
import FrameworkHearthStone.Utilities.FileUtils;
import FrameworkHearthStone.Utilities.Images;

public class PracticeChooseAdventure extends BaseScreen
{
	private static final String imagesFolder = Images.practiceChooseAdventureFolder;
	private static Pattern rootElement = FileUtils.getPattern(imagesFolder, Images.practiceChooseAdventure);

	public PracticeChooseAdventure(Region gameWindow) {
		super(rootElement, "PracticeChooseAdventure", gameWindow);
	}
	
	public void selectDifficulty(adventureDifficulty difficulty)
	{
		Framework.Logger.log("selectDifficulty: " + difficulty.toString());
		if(difficulty == adventureDifficulty.normal)
		{
			clickNormal();
		}
		else if(difficulty == adventureDifficulty.expert)
		{
			clickExpert();
		}
	}
	
	private void clickNormal() {
			SikuliWrap.clickRandomLocation(FileUtils.getPattern(imagesFolder, Images.normalButton), gameWindow);
	}
	
	private void clickExpert() {
		SikuliWrap.clickRandomLocation(FileUtils.getPattern(imagesFolder, Images.expertButton), gameWindow);
	}
	
	public void clickChoose() {
		Framework.Logger.log("clickChoose");
		SikuliWrap.clickRandomLocation(FileUtils.getPattern(imagesFolder, Images.adventureChooseButton), gameWindow);
	}
}

package FrameworkHearthStone.DataStructures;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.sikuli.script.Match;

import FrameworkHearthStone.DataStructures.Enums.classes;

public class CardData {
	public String cardArtBigImage = "";
	public String cardArtBoardImage = "";
	public String cardManaArtBigImage = "";
	public Match match;
	
	public Boolean keepCardInMulligan = false;
	
	private String name = "";
	public String manaCost = "99";
	public String type = "";
	public String subtype = "";
	public List<String> ability = new ArrayList<String>();
	public CardDataBattlecry battlecry = new CardDataBattlecry();
	public String playOrder = "99";
	public classes cardClass;
	
	public boolean canAttack = false;
	
	public int attack;
	public int health;
	
	public int aiPoints = 0;
	
	public void setName(String name)
	{
		UUID guid = UUID.randomUUID();
		this.name = name + "_" + guid.toString();
	}
	
	public String getName()
	{
		String returnName;
		if(name != null)
		{
			if(name.contains("_"))
			{
				returnName = name.substring(0, name.indexOf("_"));
			}
			else
			{
				Framework.Logger.log(name + ": didn't have a guid attached");
				returnName = name;
			}
		}
		else
		{
			returnName = "Unknown";
		}
		
		return returnName;
	}
	
	public String getNameWithGuid()
	{
		return name;
	}
}



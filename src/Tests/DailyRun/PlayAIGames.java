package Tests.DailyRun;

import org.junit.Test;

import FrameworkHearthStone.HearthstoneEnums.adventureDifficulty;
import FrameworkHearthStone.ProcessManagement.StartHearthstone;
import FrameworkHearthStone.DataStructures.AccountData;
import FrameworkHearthStone.DataStructures.Enums.classes;
import Tests.BaseTest;

public class PlayAIGames extends BaseTest
{
	@Test
	public void StartDailyGames()
	{
		for(AccountData account : hearthstone().accounts().accounts)
		{
			StartHearthstone.loadGame(account.accountName, account.password);
			hearthstone().actions().navigationActions().loadToMainMenu();
			for(classes heroClass : classes.values())
			{
				for(int i=0; i<20; i++)
				{
					hearthstone().actions().navigationActions().PlayAIGame(heroClass, adventureDifficulty.normal);
				}
			}
		}
	}
}

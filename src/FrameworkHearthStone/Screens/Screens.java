package FrameworkHearthStone.Screens;

import org.sikuli.script.Region;

import FrameworkHearthStone.DataStructures.Enums.classes;
import FrameworkHearthStone.Screens.Overlays.GameHUD;
import FrameworkHearthStone.Utilities.WindowUtils;

public class Screens
{
	Region gameWindow;
	MainMenu mainMenu;
	SeasonRewards seasonRewards;
	QuestLog questLog;
	GameHUD gameHUD;
	TodaysQuests todaysQuests;
	Mulligan mulligan;
	Board board;
	PlayScreen playScreen;
	PracticeChooseDeck practiceChooseDeck;
	PracticeChooseAdventure practiceChooseAdventure;
	
	public Screens()
	{
		gameWindow = WindowUtils.getGameWindow();
	}
	
	public MainMenu mainMenu()
	{
		if(mainMenu == null)
		{
			mainMenu = new MainMenu(gameWindow);
		}
		return mainMenu;
	}
	
	public SeasonRewards seasonRewards()
	{
		if(seasonRewards == null)
		{
			seasonRewards = new SeasonRewards(gameWindow);
		}
		return seasonRewards;
	}
	
	public QuestLog questLog()
	{
		if(questLog == null)
		{
			questLog = new QuestLog(gameWindow);
		}
		return questLog;
	}
	
	public GameHUD gameHUD()
	{
		if(gameHUD == null)
		{
			gameHUD = new GameHUD(gameWindow);
		}
		return gameHUD;
	}
	
	public TodaysQuests todaysQuests()
	{
		if(todaysQuests == null)
		{
			todaysQuests = new TodaysQuests(gameWindow);
		}
		return todaysQuests;
	}
	
	public Mulligan mulligan()
	{
		if(mulligan == null)
		{
			mulligan = new Mulligan(gameWindow);
		}
		return mulligan;
	}
	
	public Board board(classes playerClass)
	{
		if(board == null)
		{
			board = new Board(playerClass, gameWindow);
		}
		return board;
	}
	
	public PlayScreen playScreen()
	{
		if(playScreen == null)
		{
			playScreen = new PlayScreen(gameWindow);
		}
		return playScreen;
	}
	
	public PracticeChooseDeck practiceChooseDeck()
	{
		if(practiceChooseDeck == null)
		{
			practiceChooseDeck = new PracticeChooseDeck(gameWindow);
		}
		return practiceChooseDeck;
	}
	
	public PracticeChooseAdventure practiceChooseAdventure()
	{
		if(practiceChooseAdventure == null)
		{
			practiceChooseAdventure = new PracticeChooseAdventure(gameWindow);
		}
		return practiceChooseAdventure;
	}
}

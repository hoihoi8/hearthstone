package Tests.Screens;

import org.junit.Test;
import org.sikuli.script.Region;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Location;
import org.sikuli.script.Match;

import FrameworkHearthStone.Utilities.SikuliDrawUtils;
import FrameworkHearthStone.Utilities.WindowUtils;

public class ClickCoordinatesTest {
	@Test
	public void clickCoordinate()
	{
		Region gameWindow = WindowUtils.getGameWindow();
		try {
			int x = gameWindow.x + 100;
			int y = gameWindow.y + 100;
			Location location = new Location(x,y);
			gameWindow.click(location);
			SikuliDrawUtils.DrawRectangleAroundRegion(x,y, 2, 2, 3);
		} catch (FindFailed e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

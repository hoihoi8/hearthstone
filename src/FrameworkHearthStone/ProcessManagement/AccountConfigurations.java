package FrameworkHearthStone.ProcessManagement;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import FrameworkHearthStone.DataStructures.AccountData;

public class AccountConfigurations
{
	private final String accountConfigFilename = System.getProperty("user.dir") + "\\Config\\AccountConfig.txt";
	
	public List<AccountData> accounts = new ArrayList<AccountData>();
	
	public AccountConfigurations()
	{
		JSONParser parser = new JSONParser();
		 
        try
        {
            Object obj = parser.parse(new FileReader(accountConfigFilename));
 
            JSONObject jsonObject = (JSONObject) obj;
            JSONArray accountArray = (JSONArray) jsonObject.get("Accounts");
            for (int i = 0; i < accountArray.size(); i++) {
                JSONObject jsonAccount = (JSONObject)accountArray.get(i);
                AccountData loadedCard = new AccountData((String)jsonAccount.get("login"), (String)jsonAccount.get("password"));
                accounts.add(loadedCard);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
	}
	
}

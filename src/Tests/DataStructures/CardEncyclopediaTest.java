package Tests.DataStructures;

import org.junit.Test;

import FrameworkHearthStone.DataStructures.CardEncyclopedia;

public class CardEncyclopediaTest {

	@Test
	public void verifyCardDataLoads() {
		CardEncyclopedia encyclopedia = new CardEncyclopedia();
		Framework.Validate.isTrue("Cards didn't load properly from JSON.", encyclopedia.cardDictionary.get("chillwindyeti").manaCost.equals("4"));
		Framework.Validate.isTrue("Cards didn't load properly from JSON.", encyclopedia.cardDictionary.get("elvenarcher").battlecry.damage.equals("1"));
		Framework.Validate.isTrue("Cards didn't load properly from JSON.", encyclopedia.cardDictionary.get("ironfurgrizzly").ability.contains("taunt"));
	}

}

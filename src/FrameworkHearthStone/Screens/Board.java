package FrameworkHearthStone.Screens;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import org.sikuli.script.FindFailed;
import org.sikuli.script.Match;
import org.sikuli.script.Pattern;
import org.sikuli.script.Region;

import Framework.Logger;
import Framework.SikuliWrap;
import FrameworkHearthStone.DataStructures.CardData;
import FrameworkHearthStone.DataStructures.CardEncyclopedia;
import FrameworkHearthStone.DataStructures.Enums.classes;
import FrameworkHearthStone.DataStructures.GameData;
import FrameworkHearthStone.Modules.AI;
import FrameworkHearthStone.Modules.BoardIdleManager;
import FrameworkHearthStone.Modules.BoardManager;
import FrameworkHearthStone.Modules.HandManager;
import FrameworkHearthStone.Utilities.ClassMapper;
import FrameworkHearthStone.Utilities.ColorUtils;
import FrameworkHearthStone.Utilities.FileUtils;
import FrameworkHearthStone.Utilities.Images;
import FrameworkHearthStone.Utilities.ListUtils;
import FrameworkHearthStone.Utilities.ScreenshotUtils;
import FrameworkHearthStone.Utilities.SikuliDrawUtils;
import FrameworkHearthStone.Utilities.SleepUtils;

public class Board extends BaseScreen
{
	private static final String imageFolder = Images.boardFolder;
	
	public CardEncyclopedia cardEncyclopedpia; 
	private HandManager hand;
	private AI ai;
	private BoardManager boardManager;
	public GameData gameData;
	private BoardIdleManager boardIdleManager;
	
	private classes playerClass = null;
	private classes opponentsClass = null;
	
	private Region handRegion = null;
	private Region manaRegion = null;
	private Region opponentsBoardRegion = null;
	private Region opponentsFaceRegion = null;
	private Region playersBoardRegion = null;
	private Region battleCryPlayRegion = null;
	private Region playerWeaponRegion = null;
	private Region opponentsTauntRegion = null;
	private Region playerSecretRegion = null;
	private Region tauntTextRegion = null;
	private Region turnButtonRegion = null;
	private Region heroPowerRegion = null;
	private Region gameOverRegion = null;
	private Match heroPowerMatch = null;
	private Match heroMatch = null;
	private Match opponentsFaceMatch = null;
	
	public Board(classes playerClass, Region gameWindow) {
		super(FileUtils.getPattern(imageFolder, ClassMapper.getHeroImageFromClass(playerClass)), "Board", gameWindow);
		this.playerClass = playerClass;
		boardIdleManager = new BoardIdleManager(gameWindow);
		cardEncyclopedpia = new CardEncyclopedia();
		hand = new HandManager(cardEncyclopedpia, this.gameWindow);
		boardManager = new BoardManager(cardEncyclopedpia);
		ai = new AI(this.gameWindow);
		gameData = new GameData();
	}
	
	public void playOneGame()
	{
		verifyHandDrawAnimationComplete();
		populateGameData();

		int loopCount = 0;
		while(!isGameOver())
		{
			if(isYourTurn())
			{
				SleepUtils.waitRandomTime(3000, 3500);
				if(!isGameOver())
				{
					gameData.heroPowerMatch = getHeroPowerMatch();
					playOneTurn();
				}
				else {
					break;
				}
			}
			
			if(loopCount++ > 18000) {
				Framework.Logger.log("Maximum Cycles Reached");
				break;
			}
			
			SleepUtils.waitRandomTime(1000, 1000);
		}
		
		Framework.Logger.log("-------------------Game Over-----------------------");
	}

	public void populateGameData() {
		gameData.playerClass = playerClass;
		gameData.oppponentClass = getOpponentClass();
		gameData.handRegion = getHandRegion();
		gameData.playerBoardRegion = getPlayerBoardRegion();
		gameData.opponentBoardRegion = getOpponentsBoardRegion();
		gameData.opponentFaceRegion = getOpponentsFaceRegion();
		gameData.playerHandRegion = getHandRegion();
		gameData.opponentFace = getOpponentsFaceMatch();
		gameData.battleCryPlayRegion = getBattleCryPlayRegion();
		gameData.playerWeaponRegion = getPlayerWeaponRegion();
		gameData.opponentsTauntRegion = getOpponentsTauntRegion();
		gameData.tauntTextRegion = getTauntTextRegion();
		gameData.playerSecretRegion = getPlayerSecretRegion();
		gameData.playerPortraitRegion = Region.create(getHeroPortraitMatch().x, getHeroPortraitMatch().y, getHeroPortraitMatch().w, getHeroPortraitMatch().h/2);
		SikuliDrawUtils.DrawRectangleAroundRegion(gameData.playerPortraitRegion);
	}

	public void playOneTurn() {
		Framework.Logger.log("playOneTurn");
		
		if(isEndTurnButtonGreen()) {
			clickTurnDone();
			return;
		}
		
		gameData.mana = getAvailableMana();
		gameData.cardsInHand = hand.getHand(gameData);
		
		gameData.cardsOnBoard = boardManager.getPlayerBoardState(gameData, true);
		gameData = boardManager.getOpponentsBoardState(gameData);
		
		int silenceCount = 0;
		int silenceCost = 0;
		int deadlyPoisonCount = 0;
		for(CardData cardInHand : gameData.cardsInHand.values() )
		{
			if(cardInHand.getName().contains("ironbeakowl") ) {
				silenceCount++;
				silenceCost += Integer.parseInt(cardInHand.manaCost);
			}
			if(cardInHand.getName().contains("deadlypoison")) {
				deadlyPoisonCount++;
			}
		}
		
		if(deadlyPoisonCount == 0)
		{
			attackWithWeapon(silenceCount, silenceCost);
		}
		
		if(isGameOver()) { return; }
		
		gameData.cardsToPlayThisTurn = ai.chooseCombinationToPlay(gameData);
		
		boolean handIsDirty = false;
		
		if(ai.isCoinInList(gameData.cardsToPlayThisTurn))
    	{
    		ai.playCoin(gameData);
    		gameData.cardsToPlayThisTurn = ai.removeCoin(gameData.cardsToPlayThisTurn);
    		handIsDirty = true;
    	}
		
		sortCardsToPlayInOrder();
		for(CardData card : gameData.cardsToPlayThisTurn)
		{
			if(handIsDirty && card.getName() != "heroPower") {
				gameData.cardsInHand = hand.getHandUntilCardFound(gameData, card);
			}
			CardData currentCard = ai.playCard(card, gameData);
			SleepUtils.waitRandomTime(850, 1000);
			
			boolean getOpponentsBoard = ai.playCardBattleCryTarget(gameData, currentCard);
			if(getOpponentsBoard)
			{
				gameData = boardManager.getOpponentsBoardState(gameData);
			}
			
			handIsDirty = true;
			gameData = boardManager.getOpponentsBoardState(gameData);
			if(isGameOver()) { return; }
		}
		
		gameData.cardsOnBoard = boardManager.getPlayerBoardState(gameData, false);
		gameData = boardManager.getOpponentsBoardState(gameData);

		for(int i=0; i<3; i++)
		{
			int loopCount = 0;
			while(boardManager.countCardsThatCanAttack(gameData.cardsOnBoard) > 0)
			{
				for(CardData card : gameData.cardsOnBoard.values())
				{
					boolean cardAttacked = ai.attackCard(card, gameData);
					if(isGameOver()) { return; }
					if(cardAttacked)
					{
						gameData.cardsOnBoard = boardManager.getPlayerBoardState(gameData, false);
						gameData = boardManager.getOpponentsBoardState(gameData);
					}
				}
				if(loopCount++ > 12) {
					break;
				}
			}
			
			if(isEndTurnButtonGreen()) {
				clickTurnDone();
				return;
			}
			
			Framework.Logger.log("Safety net board check");
			gameData.cardsOnBoard = boardManager.getPlayerBoardState(gameData, false);
			gameData = boardManager.getOpponentsBoardState(gameData);
			SleepUtils.waitRandomTime(375, 400);
		}
		
		clickTurnDone();
	}

	private void attackWithWeapon(int silenceCount, int silenceCost) {
		for(CardData cardOnBoard : gameData.cardsOnBoard.values())
		{
			if(silenceCount < gameData.opponentsTaunts.values().size() && silenceCost <= gameData.mana)
			{
				boolean cardAttacked = ai.attackWeapon(cardOnBoard, gameData);
				
				if(cardAttacked)
				{
					SleepUtils.waitRandomTime(850, 1000);
					gameData.cardsOnBoard = boardManager.getPlayerBoardState(gameData, false);
					gameData = boardManager.getOpponentsBoardState(gameData);
				}
			}
		}
	}

	private void sortCardsToPlayInOrder() {
		Collections.sort(gameData.cardsToPlayThisTurn, new Comparator<CardData>(){
		     public int compare(CardData o1, CardData o2){
		         if(o1.playOrder == o2.playOrder)
		             return 0;
		         return Integer.parseInt(o1.playOrder) < Integer.parseInt(o2.playOrder) ? -1 : 1;
		     }
		});
	}
	
	public int getAvailableMana()
	{
		List<Match> matches = new ArrayList<Match>();
		try {
			Pattern pattern = new Pattern(FileUtils.getPattern(imageFolder, Images.manaCrystal)).similar(.8f);
			Iterator<Match> crystals = getManaRegion().findAll(pattern);
			matches = ListUtils.copyIteratorToList(crystals);
		} catch (FindFailed e) {
			Logger.log("Could not find the mana crystals in the ManaRegion");
		}
		Logger.log("getavailableMana: " + Integer.toString(matches.size()));
		return matches.size();
	}
	
	private void verifyHandDrawAnimationComplete()
	{
		SleepUtils.waitRandomTime(3000, 4000);
	}
	
	public boolean isGameOver() {
		Framework.Logger.log("isGameOver");
		boolean isGameOver = false;
		List<String> endGameImages = Arrays.asList(Images.Victory, Images.Defeat, Images.Reward, Images.clickToContinue, Images.goldEarned);
		for(String endGameImage : endGameImages)
		{
			if(SikuliWrap.clickRandomLocation(FileUtils.getPattern(imageFolder, endGameImage), getGameOverRegion()))
			{
				isGameOver = true;
			}
		}
		
		if(SikuliWrap.exists(gameWindow, FileUtils.getPattern(Images.playScreenFolder, Images.playTitle)) != null ||
				SikuliWrap.exists(gameWindow, FileUtils.getPattern(Images.practiceChooseDeckFolder, Images.practiceChooseDeckTitle)) != null)
		{
			Framework.Logger.log("Play or Choose Deck screen found. Ending game.");
			isGameOver = true;
		}
		
		return isGameOver;
	}
	
	private boolean isYourTurn()
	{
		Framework.Logger.log("isYourTurn");
		boolean isYourTurn = false;
		
		if(SikuliWrap.exists(getTurnButtonRegion(), FileUtils.getPattern(imageFolder, Images.endTurnButton)) != null)
		{
			isYourTurn = true;
		}
		else if(SikuliWrap.exists(getTurnButtonRegion(), FileUtils.getPattern(imageFolder, Images.endTurnGreenButton)) != null)
		{
			isYourTurn = true;
		}
		
		return isYourTurn;
	}
	
	public boolean isEndTurnButtonGreen()
	{
		Framework.Logger.log("isEndTurnButtonGreen");
		boolean isTurnButtonGreen = false;
		
		Match match = SikuliWrap.exists(getTurnButtonRegion(), FileUtils.getPattern(imageFolder, Images.endTurnGreenButton));
		if(match != null)
		{	
			if(ColorUtils.getRedValue(match, 12) < 80)
			{
				isTurnButtonGreen = true;
			}
		}
		
		return isTurnButtonGreen;
	}
	
	public void clickTurnDone()
	{
		Logger.log("clickTurnDone");
		boolean endTurnClicked = false;
		
		Pattern endTurnButton = FileUtils.getPattern(imageFolder, Images.endTurnButton);
		if(SikuliWrap.exists(getTurnButtonRegion(), endTurnButton) != null)
		{
			SikuliWrap.clickRandomLocation(endTurnButton, getTurnButtonRegion());
			endTurnClicked = true;
		}
		
		if(!endTurnClicked)
		{
			Pattern endTurnGreenButton = FileUtils.getPattern(imageFolder, Images.endTurnGreenButton);
			if(SikuliWrap.exists(getTurnButtonRegion(), endTurnGreenButton) != null)
			{
				SikuliWrap.clickRandomLocation(endTurnGreenButton, getTurnButtonRegion());
				endTurnClicked = true;
			}
		}
		
		if(endTurnClicked) {
			SleepUtils.waitRandomTime(1200, 1500); //Give extra time so button doesn't bug
			boardIdleManager.setMouseToIdle();
		}
		
		if(SikuliWrap.exists(getTurnButtonRegion(), FileUtils.getPattern(imageFolder, Images.endTurnButton)) != null
				|| SikuliWrap.exists(getTurnButtonRegion(), FileUtils.getPattern(imageFolder, Images.endTurnGreenButton)) != null)
		{
			if(!this.isGameOver())
			{
				Framework.Logger.log("Re-clicking end turn button");
				SleepUtils.waitRandomTime(1000, 1500);
				clickTurnDone();
			}
		}
		else
		{
			return;
		}
		
		SleepUtils.waitRandomTime(375, 400);
	}
	
	public Match getHeroPortraitMatch()
	{
		if(heroMatch == null)
		{
			for(int i=0; i<3; i++)
			{
				Region bottomHalfOfWindow = Region.create(gameWindow.x, gameWindow.y + (768/2), gameWindow.w, gameWindow.h/2);
				SikuliDrawUtils.DrawRectangleAroundRegion(bottomHalfOfWindow);
				
				Match match = SikuliWrap.exists(bottomHalfOfWindow, FileUtils.getPattern(imageFolder, ClassMapper.getHeroImageFromClass(gameData.playerClass)));
				if(match != null)
				{
					heroMatch = match;
					break;
				}
				
				SleepUtils.waitRandomTime(1500, 2000);
			}
		}
			
		Framework.Validate.isTrue("Could not find the hero portrait:" + FileUtils.getPattern(imageFolder, ClassMapper.getHeroImageFromClass(gameData.playerClass)), 
				heroMatch != null);
		return heroMatch;
	}
	
	private Match getHeroPowerMatch()
	{
		Pattern pattern = FileUtils.getPattern(Images.boardFolder, ClassMapper.getHeroPowerImageFromClass(gameData.playerClass));
		Match playerClass = SikuliWrap.exists(getHeroPowerRegion(), pattern);
		if(playerClass == null)
		{
			Framework.Validate.fail("Could not find the hero power. File: " + pattern.getFileURL());
		}
		heroPowerMatch = playerClass;
		
		return heroPowerMatch;
	}
	
	public Match getOpponentsFaceMatch()
	{
		boolean matchFound = false;
		if(opponentsFaceMatch == null)
		{
			for(int j=0; j<3; j++)
			{
				for(int i=0; i<Images.heroPortraits.length; i++)
				{
					Match heroPortrait = SikuliWrap.exists(getOpponentsFaceRegion(), FileUtils.getPattern(imageFolder, Images.heroPortraits[i]));
					if(heroPortrait != null)
					{
						opponentsFaceMatch = heroPortrait.getLastMatch();
						opponentsClass = ClassMapper.getClassFromHeroImage(Images.heroPortraits[i]);
						SikuliDrawUtils.DrawRectangleAroundRegion(opponentsFaceMatch);
						matchFound = true;
						break;
					}
				}
				
				if(matchFound) {
					break;
				}
			}
			
			if(opponentsFaceMatch == null)
			{
				opponentsFaceMatch = new Match(getOpponentsFaceRegion(), 0);
				opponentsFaceMatch.x = getOpponentsFaceRegion().x + (getOpponentsFaceRegion().w / 2) - 20;
				opponentsFaceMatch.y = getOpponentsFaceRegion().y + (getOpponentsFaceRegion().h / 2) - 20;
				opponentsFaceMatch.h = 40;
				opponentsFaceMatch.w = 40;
				SikuliDrawUtils.DrawRectangleAroundRegion(opponentsFaceMatch);
				ScreenshotUtils.takeScreenshot("UnknownHeroPortrait");
			}
			Framework.Validate.isTrue("Could not find opponents hero match", opponentsFaceMatch != null);
		}
		
		return opponentsFaceMatch;
	}
	
	private classes getOpponentClass()
	{
		classes returnClass = null;
		if(opponentsClass == null) {
			getOpponentsBoardRegion();
		}
		else {
			returnClass = opponentsClass;
		}
		
		return returnClass;
	}

	public Region getHandRegion()
	{
		if(handRegion == null)
		{
			handRegion = Region.create(getHeroPortraitMatch().x - 230, getHeroPortraitMatch().y + getHeroPortraitMatch().h + 15, 420, 105);
			SikuliDrawUtils.DrawRectangleAroundRegion(handRegion);
			handRegion.setAutoWaitTimeout(0);
		}
		
		return handRegion;
	}
	
	public Region getPlayerWeaponRegion() {
		if(playerWeaponRegion  == null)
		{
			playerWeaponRegion = Region.create(getHeroPortraitMatch().x - 170, getHeroPortraitMatch().y - 60, 265, 165);
			SikuliDrawUtils.DrawRectangleAroundRegion(playerWeaponRegion);
			playerWeaponRegion.setAutoWaitTimeout(0);
		}
		
		return playerWeaponRegion;
	}
	
	private Region getManaRegion()
	{
		if(manaRegion == null)
		{
			manaRegion = Region.create(getHeroPortraitMatch().x + 270, getHeroPortraitMatch().y + getHeroPortraitMatch().h + 30, 270, 60);
			SikuliDrawUtils.DrawRectangleAroundRegion(manaRegion);
			manaRegion.setAutoWaitTimeout(0);
		}

		return manaRegion;
	}
	
	public Region getPlayerBoardRegion()
	{
		if(playersBoardRegion == null)
		{
			playersBoardRegion = Region.create(getHeroPortraitMatch().x - 360, getHeroPortraitMatch().y - 140, 740, 90);
			SikuliDrawUtils.DrawRectangleAroundRegion(playersBoardRegion);
			playersBoardRegion.setAutoWaitTimeout(0);
		}
		
		return playersBoardRegion;
	}
	
	public Region getBattleCryPlayRegion()
	{
		if(battleCryPlayRegion == null)
		{
			battleCryPlayRegion = Region.create(getHeroPortraitMatch().x - 360, getHeroPortraitMatch().y - 180, 75, 130);
			SikuliDrawUtils.DrawRectangleAroundRegion(battleCryPlayRegion);
			battleCryPlayRegion.setAutoWaitTimeout(0);
		}
		
		return battleCryPlayRegion;
	}
	
	
	
	public Region getTurnButtonRegion()
	{
		if(turnButtonRegion == null)
		{
			turnButtonRegion = Region.create(getHeroPortraitMatch().x + 370, getHeroPortraitMatch().y - 265, 150, 150);
			SikuliDrawUtils.DrawRectangleAroundRegion(turnButtonRegion);
			turnButtonRegion.setAutoWaitTimeout(0);
		}
		
		return turnButtonRegion;
	}
	
	public Region getOpponentsBoardRegion()
	{
		if(opponentsBoardRegion == null)
		{
			opponentsBoardRegion = Region.create(getHeroPortraitMatch().x - 390, getHeroPortraitMatch().y - 350, 850, 190);
			SikuliDrawUtils.DrawRectangleAroundRegion(opponentsBoardRegion);
			opponentsBoardRegion.setAutoWaitTimeout(0);
		}
		
		return opponentsBoardRegion;
	}
	
	public Region getOpponentsFaceRegion()
	{
		if(opponentsFaceRegion == null)
		{
			opponentsFaceRegion = Region.create(getHeroPortraitMatch().x - 50, 
					getHeroPortraitMatch().y - 480, getHeroPortraitMatch().w + 100, getHeroPortraitMatch().h + 60);
			SikuliDrawUtils.DrawRectangleAroundRegion(opponentsFaceRegion);
			opponentsFaceRegion.setAutoWaitTimeout(0);
		}
		
		return opponentsFaceRegion;
	}
	
	public Region getHeroPowerRegion()
	{
		if(heroPowerRegion == null)
		{
			heroPowerRegion = Region.create(getHeroPortraitMatch().x + 85, getHeroPortraitMatch().y - 40, 140,140);
			SikuliDrawUtils.DrawRectangleAroundRegion(heroPowerRegion);
			heroPowerRegion.setAutoWaitTimeout(0);
		}
		
		return heroPowerRegion;
	}
	
	public Region getOpponentsTauntRegion()
	{
		if(opponentsTauntRegion == null)
		{
			opponentsTauntRegion = Region.create(getHeroPortraitMatch().x - 380, getHeroPortraitMatch().y - 309, 780, 133);
			SikuliDrawUtils.DrawRectangleAroundRegion(opponentsTauntRegion, 10);
			opponentsTauntRegion.setAutoWaitTimeout(0);
		}
		
		return opponentsTauntRegion;
	}
	
	public Region getTauntTextRegion()
	{
		if(tauntTextRegion == null)
		{
			tauntTextRegion = Region.create(getHeroPortraitMatch().x + 100, getHeroPortraitMatch().y - 280, 200, 120);
			SikuliDrawUtils.DrawRectangleAroundRegion(tauntTextRegion);
			tauntTextRegion.setAutoWaitTimeout(0);
		}
		
		return tauntTextRegion;
	}
	
	public Region getGameOverRegion()
	{
		if(gameOverRegion == null)
		{
			gameOverRegion = Region.create(getHeroPortraitMatch().x - 110, getHeroPortraitMatch().y - 160, 280, 150);
			SikuliDrawUtils.DrawRectangleAroundRegion(gameOverRegion);
			gameOverRegion.setAutoWaitTimeout(0);
		}
		
		return gameOverRegion;
	}
	
	public Region getPlayerSecretRegion()
	{
		if(playerSecretRegion == null)
		{
			playerSecretRegion = Region.create(getHeroPortraitMatch().x + 10, getHeroPortraitMatch().y - 230, 320, 370);
			SikuliDrawUtils.DrawRectangleAroundRegion(playerSecretRegion);
			playerSecretRegion.setAutoWaitTimeout(0);
		}
		
		return playerSecretRegion;
	}
}

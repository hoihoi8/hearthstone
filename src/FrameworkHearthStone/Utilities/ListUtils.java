package FrameworkHearthStone.Utilities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import FrameworkHearthStone.DataStructures.CardData;

public class ListUtils
{
	public static <T> List<T> copyIteratorToList(Iterator<T> iter) {
	    List<T> copy = new ArrayList<T>();
	    if(iter != null)
	    {
		    while (iter.hasNext())
		        copy.add(iter.next());
	    }
	    return copy;
	}
	
	public static <T> List<T> addIteratorItemsToList(Iterator<T> iter, List<T> list) {
	    while (iter.hasNext())
	        list.add(iter.next());
	    return list;
	}
	
	public static <T> List<T> copyList(List<T> list)
	{
		List<T> copy = new ArrayList<T>();
		for(T element : list)
		{
			copy.add(element);
		}
		
		return copy;
	}
	
	public static <V, K> List<V> copyHashValueToList(HashMap<K, V> hash)
	{
		List<V> copy = new ArrayList<V>();
		for(Entry<K, V> element : hash.entrySet())
		{
			copy.add(element.getValue());
		}
		
		return copy;
	}
	
	public static <K, V> HashMap<K, V> copyHashMap(HashMap<K, V> hash)
	{
		HashMap<K, V> copy = new HashMap<K, V>();
		for(Entry<K, V> element : hash.entrySet())
		{
			copy.put(element.getKey(), element.getValue());
		}
		
		return copy;
	}
	
	public static <K, V> HashMap<K, V> AddDataToHashMap(HashMap<K, V> hash, HashMap<K, V> hashMapDataToAdd)
	{
		for(Entry<K, V> element : hashMapDataToAdd.entrySet())
		{
			hash.put(element.getKey(), element.getValue());
		}
		
		return hash;
	}
	
	public static <T> void printList(List<T> list)
	{
		printList(list, "");
	}
	public static <T> void printList(List<T> list, String prefix)
	{
		for(T var : list)
		{
			Framework.Logger.log(prefix + " " + var.toString());
		}
	}
	
	public static void printCardNamesFromCardDataList(List<CardData> list, String prefix)
	{
		for(CardData card : list)
		{
			Framework.Logger.log(prefix + " " + card.getName());
		}
	}
}

package FrameworkHearthStone.Utilities;

import org.sikuli.script.Match;
import org.sikuli.script.Region;
import org.sikuli.script.Screen;

public class WindowUtils {
	public static Region getGameWindow()
	{
		Framework.Logger.log("getGameWindow");
		Region returnRegion = null;
		Screen screen = new Screen();
		Match titleBar = null;
		int foundX = 0;
		int foundY = 0;
		for(int i=0; i<20; i++)
		{
			titleBar = screen.exists(FileUtils.getPattern(Images.globalFolder, Images.windowedTitleBar2, .8f));
			if(titleBar != null)
			{
				Framework.Logger.log("GameWindow Found, Score: " + titleBar.getScore() + " X:" 
					+ titleBar.x + " Y:" + titleBar.y);
				if(foundX == titleBar.x && foundY == titleBar.y)
				{
					Framework.Logger.log("***GameWindow Location Stabalized***");
					break;
				}
				foundX = titleBar.x;
				foundY = titleBar.y;
			}
			
			SleepUtils.sleep(2000);
		}
		Framework.Validate.isTrue("Could not find the title bar", titleBar != null);
		
		returnRegion = Region.create(titleBar.x, titleBar.y + 20, 1024, 768);
		returnRegion.setAutoWaitTimeout(0);
		return returnRegion;
	}
}

package FrameworkHearthStone.Screens;

import org.sikuli.script.FindFailed;
import org.sikuli.script.Match;
import org.sikuli.script.Pattern;
import org.sikuli.script.Region;

import Framework.SikuliWrap;
import FrameworkHearthStone.DataStructures.Enums.classes;
import FrameworkHearthStone.Utilities.ClassMapper;
import FrameworkHearthStone.Utilities.FileUtils;
import FrameworkHearthStone.Utilities.Images;
import FrameworkHearthStone.Utilities.RandomUtils;
import FrameworkHearthStone.Utilities.SikuliDrawUtils;
import FrameworkHearthStone.Utilities.SleepUtils;

public class PracticeChooseDeck extends BaseScreen
{
	private static final String deckFolder = Images.playScreenFolder;
	private static final String imagesFolder = Images.practiceChooseDeckFolder;
	private static Pattern rootElement = FileUtils.getPattern(imagesFolder, Images.practiceChooseDeckTitle);
	
	public PracticeChooseDeck(Region gameWindow) {
		super(rootElement, "PracticeChooseDeck", gameWindow);
	}

	public void clickPlay(classes playerClass)
	{
		Pattern deckPattern = new Pattern(FileUtils.getPattern(deckFolder, ClassMapper.getHeroDeck(playerClass))).similar(.95f);
		if(SikuliWrap.exists(gameWindow, deckPattern) == null)
		{
			try {
				Pattern pattern = new Pattern(FileUtils.getPattern(deckFolder, ClassMapper.getHeroDeckUnselected(playerClass))).similar(.95f);
				if(SikuliWrap.exists(gameWindow, pattern) != null)
				{
					SikuliDrawUtils.DrawRectangleAroundRegion(gameWindow.getLastMatch().x + 10, 
							gameWindow.getLastMatch().y + 20, gameWindow.getLastMatch().w - 20,
							gameWindow.getLastMatch().h, 1);
					gameWindow.click(RandomUtils.getRandomLocation(gameWindow.getLastMatch().x + 10, 
							gameWindow.getLastMatch().y + 20, gameWindow.getLastMatch().w - 20,
							gameWindow.getLastMatch().h));
					SleepUtils.waitRandomTime(375, 400);
				}
			} catch (FindFailed e) {
				Framework.Validate.fail("Could not select deck");
			}
		}	
		
		SikuliWrap.clickRandomLocation(FileUtils.getPattern(imagesFolder, Images.deckChooseButton), gameWindow);
		SleepUtils.waitRandomTime(375, 400);
		
		SleepUtils.waitRandomTime(2000, 6000);
		
		Match lockedHeroAI = SikuliWrap.exists(gameWindow, FileUtils.getPattern(imagesFolder, Images.lockedHeroAI));
		if(lockedHeroAI != null)
		{
			SikuliWrap.clickRandomLocation(lockedHeroAI, gameWindow);
			SleepUtils.waitRandomTime(375, 400);
		}
		else if(lockedHeroAI == null)
		{
			SikuliWrap.clickRandomLocation(FileUtils.getPattern(imagesFolder, Images.hunterAI), gameWindow);
			SleepUtils.waitRandomTime(375, 400);
		}
		
		SikuliWrap.clickRandomLocation(FileUtils.getPattern(imagesFolder, Images.practicePlayButton), gameWindow);
		SleepUtils.waitRandomTime(375, 400);
	}
}

package Tests.Modules;

import java.util.HashMap;
import java.util.List;

import org.junit.Test;

import FrameworkHearthStone.DataStructures.CardData;
import FrameworkHearthStone.DataStructures.Enums.classes;
import FrameworkHearthStone.DataStructures.GameData;
import FrameworkHearthStone.Modules.AI;
import FrameworkHearthStone.Utilities.WindowUtils;

public class AITest {

	@Test
	public void verifyBestManaUsedChooseTwo() {
		HashMap<String, CardData> testHand = new HashMap<String, CardData>();
		CardData card1 = new CardData();
		CardData card2 = new CardData();
		CardData card3 = new CardData();
		CardData card4 = new CardData();
		card1.setName("DarkIronDwarf");
		card1.manaCost = "4";
		card1.aiPoints = 6;
		card2.setName("BloodfenRaptor");
		card2.manaCost = "2";
		card2.aiPoints = 3;
		card3.setName("AnimalCompanion");
		card3.manaCost = "3";
		card3.aiPoints = 8;
		card4.setName("HeroPower");
		card4.manaCost = "2";
		card4.aiPoints = 2;
		testHand.put(card1.getNameWithGuid(), card1);
		testHand.put(card2.getNameWithGuid(), card2);
		testHand.put(card3.getNameWithGuid(), card3);
		testHand.put(card4.getNameWithGuid(), card4);
		
		HashMap<String, CardData> testOpponentsBoard = new HashMap<String, CardData>();
		CardData cardOpp1 = new CardData();
		cardOpp1.setName("Taunt");
		cardOpp1.ability.add("taunt");
		cardOpp1.ability.add("divineShield");
		testOpponentsBoard.put(cardOpp1.getName(), cardOpp1);
		
		GameData gameData = new GameData();
		gameData.cardsOnBoard = new HashMap<String, CardData>();
		gameData.cardsInHand = testHand;
		gameData.opponentsTaunts = testOpponentsBoard;
		gameData.mana = 6;
		gameData.oppponentClass = classes.mage;
		
		AI ai = new AI(WindowUtils.getGameWindow());
		List<CardData> nameOfCardsToPlay = ai.chooseCombinationToPlay(gameData);
		printCardsPlayed(gameData.mana, nameOfCardsToPlay);
		validateCardInList("BloodfenRaptor", nameOfCardsToPlay, 1);
		validateCardInList("AnimalCompanion", nameOfCardsToPlay, 1);
	}

	@Test
	public void verifyBestManaUsedChooseThree() {
		HashMap<String, CardData> testHand = new HashMap<String, CardData>();
		CardData card1 = new CardData();
		CardData card2 = new CardData();
		CardData card3 = new CardData();
		CardData card4 = new CardData();
		CardData card5 = new CardData();
		card1.setName("DarkIronDwarf");
		card1.manaCost = "4";
		card1.aiPoints = 6;
		card2.setName("BloodfenRaptor");
		card2.manaCost = "2";
		card2.aiPoints = 3;
		card3.setName("AnimalCompanion");
		card3.manaCost = "3";
		card3.aiPoints = 8;
		card4.setName("LeperGnome");
		card4.manaCost = "1";
		card4.aiPoints = 4;
		card5.setName("HeroPower");
		card5.manaCost = "2";
		card5.aiPoints = 2;
		
		testHand.put(card1.getNameWithGuid(), card1);
		testHand.put(card2.getNameWithGuid(), card2);
		testHand.put(card3.getNameWithGuid(), card3);
		testHand.put(card4.getNameWithGuid(), card4);
		testHand.put(card5.getNameWithGuid(), card5);
		
		GameData gameData = new GameData();
		gameData.cardsOnBoard = new HashMap<String, CardData>();
		gameData.cardsInHand = testHand;
		gameData.opponentsTaunts  = new HashMap<String, CardData>();
		gameData.mana = 6;
		gameData.oppponentClass = classes.mage;
		
		AI ai = new AI(WindowUtils.getGameWindow());
		List<CardData> nameOfCardsToPlay = ai.chooseCombinationToPlay(gameData);
		printCardsPlayed(gameData.mana, nameOfCardsToPlay);
		validateCardInList("AnimalCompanion", nameOfCardsToPlay, 1);
		validateCardInList("LeperGnome", nameOfCardsToPlay, 1);
		validateCardInList("BloodfenRaptor", nameOfCardsToPlay, 1);
	}
	
	@Test
	public void verifyBestManaUsedDuplicateCard() {
		HashMap<String, CardData> testHand = new HashMap<String, CardData>();
		CardData card1 = new CardData();
		CardData card2 = new CardData();
		CardData card3 = new CardData();
		card1.setName("nightblade");
		card1.manaCost = "5";
		card1.aiPoints = 7;
		card2.setName("nightblade");
		card2.manaCost = "5";
		card2.aiPoints = 7;
		card3.setName("HeroPower");
		card3.manaCost = "2";
		card3.aiPoints = 2;
		
		testHand.put(card1.getNameWithGuid(), card1);
		testHand.put(card2.getNameWithGuid(), card2);
		testHand.put(card3.getNameWithGuid(), card3);
		
		GameData gameData = new GameData();
		gameData.cardsOnBoard = new HashMap<String, CardData>();
		gameData.cardsInHand = testHand;
		gameData.opponentsTaunts  = new HashMap<String, CardData>();
		gameData.mana = 10;
		gameData.oppponentClass = classes.mage;
		
		AI ai = new AI(WindowUtils.getGameWindow());
		List<CardData> nameOfCardsToPlay = ai.chooseCombinationToPlay(gameData);
		printCardsPlayed(gameData.mana, nameOfCardsToPlay);
		validateCardInList("nightblade", nameOfCardsToPlay, 2);
	}

	@Test
	public void verifyBestManaUsedNoCards() {
		HashMap<String, CardData> testHand = new HashMap<String, CardData>();
		CardData card1 = new CardData();
		CardData card2 = new CardData();
		CardData card3 = new CardData();
		CardData card4 = new CardData();
		CardData card5 = new CardData();
		CardData card6 = new CardData();
		CardData card7 = new CardData();
		card1.setName("razorfenhunter");
		card1.manaCost = "3";
		card1.aiPoints = 3;
		card2.setName("razorfenhunter");
		card2.manaCost = "3";
		card2.aiPoints = 3;
		card3.setName("animalcompanion");
		card3.manaCost = "3";
		card3.aiPoints = 8;
		card4.setName("bloodfenraptor");
		card4.manaCost = "2";
		card4.aiPoints = 3;
		card5.setName("ironforgerifleman");
		card5.manaCost = "3";
		card5.aiPoints = 3;
		card6.setName("nightblade");
		card6.manaCost = "5";
		card6.aiPoints = 7;
		card7.setName("HeroPower");
		card7.manaCost = "2";
		card7.aiPoints = 2;
		
		testHand.put(card1.getNameWithGuid(), card1);
		testHand.put(card2.getNameWithGuid(), card2);
		testHand.put(card3.getNameWithGuid(), card3);
		testHand.put(card4.getNameWithGuid(), card4);
		testHand.put(card5.getNameWithGuid(), card5);
		testHand.put(card6.getNameWithGuid(), card6);
		testHand.put(card7.getNameWithGuid(), card7);
		
		GameData gameData = new GameData();
		gameData.cardsOnBoard = new HashMap<String, CardData>();
		gameData.cardsInHand = testHand;
		gameData.opponentsTaunts  = new HashMap<String, CardData>();
		gameData.mana = 1;
		gameData.oppponentClass = classes.mage;
		
		AI ai = new AI(WindowUtils.getGameWindow());
		List<CardData> nameOfCardsToPlay = ai.chooseCombinationToPlay(gameData);
		printCardsPlayed(gameData.mana, nameOfCardsToPlay);
		Framework.Validate.isTrue("There should be no cards to play", nameOfCardsToPlay.size() == 0);
	}
	
	@Test
	public void verifyBestManaUsedSecretAlreadyOnBoard() {
		HashMap<String, CardData> testHand = new HashMap<String, CardData>();
		CardData card1 = new CardData();
		CardData card2 = new CardData();
		card1.setName("explosiveTrap");
		card1.manaCost = "2";
		card1.aiPoints = 4;
		card1.type = "secret";
		card2.setName("heroPower");
		card2.manaCost = "2";
		card2.aiPoints = 2;
		
		testHand.put(card1.getNameWithGuid(), card1);
		testHand.put(card2.getNameWithGuid(), card2);
		
		GameData gameData = new GameData();
		gameData.cardsOnBoard = new HashMap<String, CardData>();
		gameData.cardsOnBoard.put(card1.getNameWithGuid(), card1);
		gameData.cardsInHand = testHand;
		gameData.opponentsTaunts  = new HashMap<String, CardData>();
		gameData.mana = 2;
		gameData.oppponentClass = classes.mage;
		
		AI ai = new AI(WindowUtils.getGameWindow());
		List<CardData> nameOfCardsToPlay = ai.chooseCombinationToPlay(gameData);
		printCardsPlayed(gameData.mana, nameOfCardsToPlay);
		validateCardInList("heroPower", nameOfCardsToPlay, 1);
	}
	
	@Test
	public void verifyBestManaUsedTwoWeaponsInHand() {
		HashMap<String, CardData> testHand = new HashMap<String, CardData>();
		CardData card1 = new CardData();
		CardData card2 = new CardData();
		card1.setName("glaivezooka");
		card1.manaCost = "2";
		card1.aiPoints = 4;
		card1.type = "weapon";
		card2.setName("eaglehornbow");
		card2.manaCost = "3";
		card2.aiPoints = 6;
		card2.type = "weapon";
		
		testHand.put(card1.getNameWithGuid(), card1);
		testHand.put(card2.getNameWithGuid(), card2);
		
		GameData gameData = new GameData();
		gameData.cardsOnBoard = new HashMap<String, CardData>();
		gameData.cardsInHand = testHand;
		gameData.opponentsTaunts  = new HashMap<String, CardData>();
		gameData.mana = 3;
		gameData.oppponentClass = classes.mage;
		
		AI ai = new AI(WindowUtils.getGameWindow());
		List<CardData> nameOfCardsToPlay = ai.chooseCombinationToPlay(gameData);
		printCardsPlayed(gameData.mana, nameOfCardsToPlay);
		validateCardInList("eaglehornbow", nameOfCardsToPlay, 1);
	}
	
	void printCardsPlayed(int mana, List<CardData> cardsToPlay)
	{
		Framework.Logger.log("Mana: " + Integer.toString(mana));
		Framework.Logger.log("\n");
	}
	
	private void validateCardInList(String expectedCard, List<CardData> nameOfCardsToPlay, int expectedCount) {
		int count = 0;
		for(CardData card : nameOfCardsToPlay)
		{
			if(card.getName().equals(expectedCard))
			{
				count++;
			}
		}
		Framework.Validate.isTrue("Card expected in hand:" + expectedCard, count == expectedCount);
	}
}

package FrameworkHearthStone.DataStructures;

public class AccountData {
	public String accountName;
	public String password;
	
	public AccountData(String accountName, String password)
	{
		this.accountName = accountName;
		this.password = password;
	}
}

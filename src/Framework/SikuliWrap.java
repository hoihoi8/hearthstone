package Framework;

import org.sikuli.script.FindFailed;
import org.sikuli.script.Location;
import org.sikuli.script.Match;
import org.sikuli.script.Pattern;
import org.sikuli.script.Region;
import org.sikuli.script.Screen;
import FrameworkHearthStone.Utilities.RandomUtils;

public class SikuliWrap {
	public static boolean verboseLogging = false;
	
	public static Match verifyExists(Region region, Pattern pattern)
	{
		return verifyExists(region, pattern, verboseLogging);
	}
	public static Match verifyExists(Region region, Pattern pattern, boolean verboseLogging)
	{
		Match match = region.exists(pattern);
		
		if(verboseLogging && match == null)
		{
			Pattern newPattern = new Pattern(pattern).similar(.5f);
			if(region.exists(newPattern) != null)
			{
				Framework.Logger.log(pattern.getFileURL().toString() + "/nClosest Match Score: " + region.getLastMatch().getScore() + ", Locaction X/Y:"
						+ region.getLastMatch().x + "," + region.getLastMatch().y);
			}
			else
			{
				Framework.Logger.log("No match greater than .5f");
			}
		}
		Framework.Validate.isTrue("VerifyExists: Fail: " + pattern.getFileURL().toString(), match != null);
		return match;
	}

	public static Match exists(Region region, Pattern pattern)
	{
		return exists(region, pattern, verboseLogging);
	}
	public static Match exists(Region region, Pattern pattern, boolean verboseLogging)
	{
		Match match = region.exists(pattern);
		
		if(verboseLogging && match == null)
		{
			Pattern newPattern = new Pattern(pattern).similar(.5f);
			if(region.exists(newPattern) != null)
			{
				Framework.Logger.log("Closest Match Score: " + region.getLastMatch().getScore() + ", Locaction X/Y:"
						+ region.getLastMatch().x + "," + region.getLastMatch().y);
			}
			else
			{
				Framework.Logger.log("No match greater than .5f");
			}
		}
		return match;
	}
	
	public static boolean wait(Screen screen, Pattern pattern)
	{
		return wait(screen, pattern, 30, verboseLogging);
	}
	public static boolean wait(Screen screen, Pattern pattern, int seconds)
	{
		return wait(screen, pattern, seconds, verboseLogging);
	}
	public static boolean wait(Screen screen, Pattern pattern, int seconds, boolean verboseLogging)
	{
		boolean exists = false;
		try {
			screen.wait(pattern, seconds);
			exists = true;
		}
		catch (FindFailed e) {
			exists = false;
			Pattern newPattern = new Pattern(pattern).similar(.5f);
			if(screen.exists(newPattern) != null)
			{
				Framework.Logger.log("Closest Match Score: " + screen.getLastMatch().getScore() + ", Locaction X/Y:"
						+ screen.getLastMatch().x + "," + screen.getLastMatch().y);
			}
			else
			{
				Framework.Logger.log("No match greater than .5f");
			}
		}
		return exists;
	}
	
	public static boolean wait(Region region, Pattern pattern)
	{
		return wait(region, pattern, 30, verboseLogging);
	}
	public static boolean wait(Region region, Pattern pattern, int seconds)
	{
		return wait(region, pattern, seconds, verboseLogging);
	}
	public static boolean wait(Region region, Pattern pattern, int seconds, boolean verboseLogging)
	{
		boolean exists = false;
		try {
			region.wait(pattern, seconds);
			exists = true;
		}
		catch (FindFailed e) {
			exists = false;
			Pattern newPattern = new Pattern(pattern).similar(.5f);
			if(region.exists(newPattern) != null)
			{
				Framework.Logger.log("	Closest Match Score: " + region.getLastMatch().getScore() + ", Locaction X/Y:"
						+ region.getLastMatch().x + "," + region.getLastMatch().y);
			}
			else
			{
				Framework.Logger.log("	No match greater than .5f");
			}
		}
		return exists;
	}
	
	public static boolean clickRandomLocation(Pattern pattern, Region clickRegion)
	{
		boolean clicked = false;
		try {
			Match match = SikuliWrap.exists(clickRegion, pattern);
			if(match != null) {
				clickRegion.click(RandomUtils.getRandomLocation(match));
				clicked = true;
			}
			else {
				throw new FindFailed("Click: Pattern not found");
			}
		} catch (FindFailed e) {
			Framework.Validate.fail("Click: Could not click pattern");
		}
		
		return clicked;
	}
	
	public static boolean clickRandomLocation(Match match, Region clickRegion)
	{
		boolean clicked = false;
		try {
			clickRegion.click(RandomUtils.getRandomLocation(match));
			clicked = true;
		} catch (FindFailed e) {
			Framework.Validate.fail("Click: Could not click match");
		}
		
		return clicked;
	}
	
	public static boolean clickLocation(Location location, Region clickRegion)
	{
		boolean clicked = false;
		try {
				clickRegion.click(location);
				clicked = true;
		} catch (FindFailed e) {
			Framework.Validate.fail("Click: Could not click location");
		}
		
		return clicked;
	}
	
	public static boolean hoverRandomLocation(Pattern pattern, Region clickRegion)
	{
		boolean hovered = false;
		try {
			Match match = SikuliWrap.exists(clickRegion, pattern);
			if(match != null) {
				clickRegion.hover(RandomUtils.getRandomLocation(match));
				hovered = true;
			}
			else {
				throw new FindFailed("Hover: Pattern not found");
			}
		} catch (FindFailed e) {
			Framework.Validate.fail("Hover: Could not hover pattern");
		}
		
		return hovered;
	}
}

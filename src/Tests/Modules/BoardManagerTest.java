package Tests.Modules;

import java.util.HashMap;

import org.junit.Test;
import org.sikuli.script.Region;

import FrameworkHearthStone.DataStructures.CardData;
import FrameworkHearthStone.DataStructures.CardEncyclopedia;
import FrameworkHearthStone.DataStructures.GameData;
import FrameworkHearthStone.Modules.BoardManager;

public class BoardManagerTest {
	//CreaturesOnPlayerBoard.png
//	@Test
//	public void findCreaturesOnBoard() {
//		Region region = Region.create(0,250, 1024, 500);
//		region.setAutoWaitTimeout(0);
//		BoardManager boardManager = new BoardManager();
//		int size = boardManager.getPlayerBoardState(region).size();
//		Framework.Validate.isTrue("Wrong number of cards found. Expected: 4 Actual:" + Integer.toString(size), size == 6);
//	}
//	
//	//CreaturesOnPlayerBoard.png
//	@Test
//	public void findAttackingCreaturesOnBoard() {
//		Region region = Region.create(0,250, 1024, 500);
//		region.setAutoWaitTimeout(0);
//		BoardManager boardManager = new BoardManager();
//		HashMap<String, CardData> testCards = new HashMap<String, CardData>();
//		testCards = boardManager.getAttackingCards(region);
//		Framework.Validate.isTrue("Wrong number of unknown cards found. Expected: 4 Actual: " + Integer.toString(testCards.size()), testCards.size() == 4);
//	}
//	
	//TauntsOnBoard.PNG
	@Test
	public void findTauntCreatures() {
		GameData gameData = new GameData();
		gameData.opponentBoardRegion = Region.create(0, 0, 1024, 1000);
		gameData.opponentBoardRegion.setAutoWaitTimeout(0);
		BoardManager boardManager = new BoardManager(new CardEncyclopedia());
		HashMap<String, CardData> testCards = new HashMap<String, CardData>();
		gameData = boardManager.getOpponentsBoardState(gameData);
		Framework.Validate.isTrue("Wrong number of taunt creatures", testCards.size() == 3);
	}
}

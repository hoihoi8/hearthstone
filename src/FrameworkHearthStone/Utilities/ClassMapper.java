package FrameworkHearthStone.Utilities;

import FrameworkHearthStone.DataStructures.CardData;
import FrameworkHearthStone.DataStructures.CardDataBattlecry;
import FrameworkHearthStone.DataStructures.GameData;
import FrameworkHearthStone.DataStructures.Enums.classes;

public class ClassMapper {

	public static String getHeroImageFromClass(classes playerClass) {
		String returnImage = null;
		if(playerClass == classes.hunter) {
			returnImage = Images.hunterHero;
		}
		else if(playerClass == classes.paladin) {
			returnImage = Images.palidanHero;
		}
		else if(playerClass == classes.rogue) {
			returnImage = Images.rougeHero;
		}
		else if(playerClass == classes.warlock) {
			returnImage = Images.warlockHero;
		}
		else if(playerClass == classes.druid) {
			returnImage = Images.druidHero;
		}
		else if(playerClass == classes.mage) {
			returnImage = Images.mageHero;
		}
		else if(playerClass == classes.shaman) {
			returnImage = Images.shamanHero;
		}
		else if(playerClass == classes.warrior) {
			returnImage = Images.warriorHero;
		}
		else if(playerClass == classes.priest) {
			returnImage = Images.priestHero;
		}
		Framework.Validate.isTrue("getHeroImageFromClass: Class not supported", returnImage != null);
		return returnImage;
	}
	
	public static classes getClassFromHeroImage(String portraitImageName) {
		classes returnClass = null;
		switch(portraitImageName)
		{
		case Images.hunterHero:
			returnClass = classes.hunter;
			break;
		case Images.druidHero:
			returnClass = classes.druid;
			break;
		case Images.mageHero:
			returnClass = classes.mage;
			break;
		case Images.palidanHero:
			returnClass = classes.paladin;
			break;
		case Images.priestHero:
			returnClass = classes.priest;
			break;
		case Images.rougeHero:
			returnClass = classes.rogue;
			break;
		case Images.shamanHero:
			returnClass = classes.shaman;
			break;
		case Images.warlockHero:
			returnClass = classes.warlock;
			break;
		case Images.warriorHero:
			returnClass = classes.warrior;
			break;
		}
		Framework.Validate.isTrue("getClassFromHeroImage: Image couldn't be mapped to hero", returnClass != null);
		
		return returnClass;
	}

	public static String getHeroPowerImageFromClass(classes playerClass) {
		String returnImage = null;
		if(playerClass == classes.hunter) {
			returnImage = Images.hunterHeroPowerButton;
		}
		else if(playerClass == classes.paladin) {
			returnImage = Images.palidanHeroPowerButton;
		}
		else if(playerClass == classes.rogue) {
			returnImage = Images.rogueHeroPowerButton;
		}
		else if(playerClass == classes.warlock) {
			returnImage = Images.warlockHeroPowerButton;
		}
		else if(playerClass == classes.druid) {
			returnImage = Images.druidHero;
		}
		else if(playerClass == classes.mage) {
			returnImage = Images.mageHeroPowerButton;
		}
		else if(playerClass == classes.shaman) {
			returnImage = Images.shamanHeroPowerButton;
		}
		else if(playerClass == classes.warrior) {
			returnImage = Images.warriorHeroPowerButton;
		}
		else if(playerClass == classes.priest) {
			returnImage = Images.priestHeroPowerButton;
		}
		Framework.Validate.isTrue("getHeroPowerImageFromClass: Class not supported yet", returnImage != null);
		return returnImage;
	}

	public static String getHeroDeck(classes playerClass) {
		String returnImage = null;
		if(playerClass == classes.hunter) {
			returnImage = Images.hunterDeck;
		}
		else if(playerClass == classes.paladin) {
			returnImage = Images.palidanDeck;
		}
		else if(playerClass == classes.rogue) {
			returnImage = Images.rogueDeck;
		}
		else if(playerClass == classes.warlock) {
			returnImage = Images.warlockdeck;
		}
		else if(playerClass == classes.druid) {
			returnImage = Images.druidDeck;
		}
		else if(playerClass == classes.mage) {
			returnImage = Images.mageDeck;
		}
		else if(playerClass == classes.shaman) {
			returnImage = Images.shamanDeck;
		}
		else if(playerClass == classes.warrior) {
			returnImage = Images.warriordeck;
		}
		else if(playerClass == classes.priest) {
			returnImage = Images.priestDeck;
		}
		Framework.Validate.isTrue("getHeroDeck: Class not supported yet", returnImage != null);
		return returnImage;
	}

	public static String getHeroDeckUnselected(classes playerClass) {
		String returnImage = null;
		if(playerClass == classes.hunter) {
			returnImage = Images.hunterDeckUnselected;
		}
		else if(playerClass == classes.paladin) {
			returnImage = Images.palidanDeckUnselected;
		}
		else if(playerClass == classes.rogue) {
			returnImage = Images.rogueDeckUnselected;
		}
		else if(playerClass == classes.warlock) {
			returnImage = Images.warlockDeckUnselected;
		}
		else if(playerClass == classes.druid) {
			returnImage = Images.druidDeckUnselected;
		}
		else if(playerClass == classes.mage) {
			returnImage = Images.mageDeckUnselected;
		}
		else if(playerClass == classes.shaman) {
			returnImage = Images.shamanDeckUnselected;
		}
		else if(playerClass == classes.warrior) {
			returnImage = Images.warriorDeckUnselected;
		}
		else if(playerClass == classes.priest) {
			returnImage = Images.priestDeckUnselected;
		}
		Framework.Validate.isTrue("getHeroDeckUnselected: Class not supported yet", returnImage != null);
		return returnImage;
	}
	
	public static classes getClassFromString(String classString) {
		classes returnClass = null;
		switch(classString)
		{
		case "hunter":
			returnClass = classes.hunter;
			break;
		case "druid":
			returnClass = classes.druid;
			break;
		case "mage":
			returnClass = classes.mage;
			break;
		case "palidan":
			returnClass = classes.paladin;
			break;
		case "priest":
			returnClass = classes.priest;
			break;
		case "rogue":
			returnClass = classes.rogue;
			break;
		case "shaman":
			returnClass = classes.shaman;
			break;
		case "warlock":
			returnClass = classes.warlock;
			break;
		case "warrior":
			returnClass = classes.warrior;
			break;
		case "neutral":
			returnClass = classes.neutral;
		}
		Framework.Validate.isTrue("getClassFromString: String couldn't be mapped to hero", returnClass != null);
		
		return returnClass;
	}
	
	public static int getHeroPowerValue(classes playerClass)
	{
		int returnValue = -1;
		if(playerClass == classes.hunter) {
			returnValue = 2;
		}
		else if(playerClass == classes.paladin) {
			returnValue = 1;
		}
		else if(playerClass == classes.rogue) {
			returnValue = 2;
		}
		else if(playerClass == classes.warlock) {
			returnValue = 2;
		}
		else if(playerClass == classes.druid) {
			returnValue = 1;
		}
		else if(playerClass == classes.mage) {
			returnValue = 1;
		}
		else if(playerClass == classes.shaman) {
			returnValue = 1;
		}
		else if(playerClass == classes.warrior) {
			returnValue = 1;
		}
		else if(playerClass == classes.priest) {
			returnValue = 1;
		}
		Framework.Validate.isTrue("getHeroPowerValue: Class not supported", returnValue != -1);
		return returnValue;
	}
	
	public static CardData getHeroPowerForHand(GameData gameData)
	{
		CardData heroPower = new CardData();
		heroPower.setName("heroPower");
		heroPower.manaCost = "2";
		heroPower.match = gameData.heroPowerMatch;
		heroPower.aiPoints = ClassMapper.getHeroPowerValue(gameData.playerClass);
		heroPower.type = "heroPower";
		heroPower.playOrder = "10";
		heroPower.battlecry = new CardDataBattlecry();
		if(gameData.playerClass == classes.priest) {
			heroPower.battlecry.target = "self";
			heroPower.battlecry.aiPoints = 0;
		}
		else if(gameData.playerClass == classes.mage)
		{
			heroPower.battlecry.target = "enemy";
			heroPower.battlecry.aiPoints = 0;
		}
		else if(gameData.playerClass == classes.rogue)
		{
			heroPower.battlecry.target = "existingWeapon";
			heroPower.battlecry.aiPoints = -3;
		}
		else
		{
			heroPower.battlecry.target = "none";
			heroPower.battlecry.aiPoints = 0;
		}

		return heroPower;
	}
}

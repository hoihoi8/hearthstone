package FrameworkHearthStone.Utilities;

import java.util.Random;

public class SleepUtils {
	public static void waitRandomTime(int minMilliSeconds, int maxMilliSeconds)
	{
		Random rand = new Random();
		try {
			Thread.sleep(rand.nextInt(maxMilliSeconds) + minMilliSeconds);
		} catch (InterruptedException e) {}
	}
	
	public static void sleep(int milliSeconds)
	{
		try {
			Thread.sleep(milliSeconds);
		} catch (InterruptedException e) {}
	}
}

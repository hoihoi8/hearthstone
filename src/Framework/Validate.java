package Framework;
import static org.junit.Assert.*;
import FrameworkHearthStone.Utilities.ScreenshotUtils;

public class Validate {
	public static void isTrue(String message, boolean test)
	{
		if(!test) {
			handleFailure(message);
		}
		assertTrue(message, test);
	}

	private static void handleFailure(String message) {
		Framework.Logger.log(message);
		ScreenshotUtils.takeScreenshot("Failure");
	}
	
	public static void isFalse(String message, boolean test)
	{
		if(test) {
			handleFailure(message);
		}
		assertTrue(message, !test);
	}
	
	public static void fail(String message)
	{
		handleFailure(message);
		org.junit.Assert.fail("!!!Fail:" + message);
	}
}
	
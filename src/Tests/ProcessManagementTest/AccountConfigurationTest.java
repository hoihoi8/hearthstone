package Tests.ProcessManagementTest;

import org.junit.Test;

import FrameworkHearthStone.DataStructures.AccountData;
import FrameworkHearthStone.ProcessManagement.AccountConfigurations;

public class AccountConfigurationTest {
	@Test
	public void verifyLoadedAccounts()
	{
		AccountConfigurations accountConfiguration = new AccountConfigurations();
		AccountData account1 = accountConfiguration.accounts.get(0);
		AccountData account2 = accountConfiguration.accounts.get(1);
		
//		{
//		"login":"WeaponJustice@gmail.com",
//		"password":"weaponJustice1"
//		},
//		{
//		"login":"probablesquire@gmail.com",
//		"password":"hearthstone1908"
//		}
			
		Framework.Validate.isTrue("account name is wrong. Expected: WeaponJustice@gmail.com, Actual: " + account1.accountName, account1.accountName.equals("WeaponJustice@gmail.com"));
		Framework.Validate.isTrue("account name is wrong. Expected: probablesquire@gmail.com, Actual: " + account2.accountName, account2.accountName.equals("probablesquire@gmail.com"));
		
		Framework.Validate.isTrue("password is wrong. Expected: weaponJustice1, Actual: " + account1.password, account1.password.equals("weaponJustice1"));
		Framework.Validate.isTrue("password is wrong. Expected: hearthstone1908, Actual: " + account1.password, account2.password.equals("hearthstone1908"));
	}
}

package FrameworkHearthStone.ProcessManagement;

import java.io.IOException;

import org.sikuli.script.*;

import Framework.SikuliWrap;
import FrameworkHearthStone.Utilities.FileUtils;
import FrameworkHearthStone.Utilities.SleepUtils;

public class StartHearthstone {
	private static String launcherFolder = "\\ProcessManagement\\Launcher\\";
	private static String battleNetPath = "\"C:\\Program Files\\Battle.net\\Battle.net Launcher.exe\"";
    private static String hearthstoneIconImage = "HearthstoneIcon.png";
    private static String playButtonImage = "PlayButton.png";
    private static String loginDialogImage = "loginDialog.png";
    private static String passwordTextBoxImage = "password.png";
    private static String loginButtonImage = "loginButton.png";
	
	public static boolean loadGame(String username, String password)
	{
		Framework.Logger.log("Load Hearthstone and login. Account: " + username + " Password: " + password);
    	boolean gameLoaded = false;
    	String[] args = {"cmd","/c",battleNetPath};
    	Runtime rt = Runtime.getRuntime();
    	ProcessBuilder pb = new ProcessBuilder(args);
    	try {
			Process pr = pb.start();
		} catch (IOException e1) {
			Framework.Validate.fail("Failed to start process:" + e1.getMessage());
			e1.printStackTrace();
		}   
		
        Screen screen = new Screen();
        try{
        	if(SikuliWrap.wait(screen, FileUtils.getPattern(launcherFolder, loginDialogImage)))
        	{
        		screen.click(FileUtils.getPattern(launcherFolder, passwordTextBoxImage));
        		SleepUtils.waitRandomTime(375, 400);
        		screen.type(FileUtils.getPattern(launcherFolder, passwordTextBoxImage), password);
        		SleepUtils.waitRandomTime(375, 400);
        		
        		Match passwordBox = screen.getLastMatch();
        		Region userNameBox = Region.create(passwordBox.x + passwordBox.w - 5, passwordBox.y - passwordBox.h, 5,5);
        		screen.click(userNameBox);
        		screen.type(userNameBox, Key.BACKSPACE+Key.BACKSPACE+Key.BACKSPACE+Key.BACKSPACE+
        				Key.BACKSPACE+Key.BACKSPACE+Key.BACKSPACE+Key.BACKSPACE+Key.BACKSPACE+Key.BACKSPACE+
        				Key.BACKSPACE+Key.BACKSPACE+Key.BACKSPACE+Key.BACKSPACE+Key.BACKSPACE+Key.BACKSPACE+
        				Key.BACKSPACE+Key.BACKSPACE+Key.BACKSPACE+Key.BACKSPACE+Key.BACKSPACE+Key.BACKSPACE+
        				Key.BACKSPACE+Key.BACKSPACE+Key.BACKSPACE+Key.BACKSPACE+Key.BACKSPACE+Key.BACKSPACE+
        				Key.BACKSPACE+Key.BACKSPACE+Key.BACKSPACE+Key.BACKSPACE+Key.BACKSPACE+Key.BACKSPACE+
        				Key.BACKSPACE+Key.BACKSPACE+Key.BACKSPACE+Key.BACKSPACE+Key.BACKSPACE+Key.BACKSPACE);
        		screen.type(userNameBox, username);
        		
        		screen.click(FileUtils.getPattern(launcherFolder, loginButtonImage));
        		SleepUtils.waitRandomTime(375, 400);
        	}
        	else
        	{
        		Framework.Validate.fail("Could not find login dialog");
        	}
        	
        	screen.wait(FileUtils.getPattern(launcherFolder, hearthstoneIconImage), 15);
        	SleepUtils.waitRandomTime(375, 400);
        	screen.click(FileUtils.getPattern(launcherFolder, hearthstoneIconImage));
        	SleepUtils.waitRandomTime(375, 400);
        	screen.click(FileUtils.getPattern(launcherFolder, playButtonImage));
        	SleepUtils.waitRandomTime(375, 400);
        	gameLoaded = true;
        }
        catch(Exception e){
        	Framework.Logger.log(e.getMessage());
        }
        
        return gameLoaded;
    }
	
	public static void closeBattleNet()
	{
		App.close("Battle.net");
	}
}

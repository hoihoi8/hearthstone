package FrameworkHearthStone.Screens;

import org.sikuli.script.FindFailed;
import org.sikuli.script.Match;
import org.sikuli.script.Pattern;
import org.sikuli.script.Region;

import Framework.SikuliWrap;
import FrameworkHearthStone.DataStructures.Enums.classes;
import FrameworkHearthStone.Screens.Overlays.Settings;
import FrameworkHearthStone.Utilities.ClassMapper;
import FrameworkHearthStone.Utilities.FileUtils;
import FrameworkHearthStone.Utilities.Images;
import FrameworkHearthStone.Utilities.RandomUtils;
import FrameworkHearthStone.Utilities.SikuliDrawUtils;

public class PlayScreen extends BaseScreen
{
	private static final String imagesFolder = Images.playScreenFolder;
	private static Pattern rootElement = FileUtils.getPattern(imagesFolder, Images.playTitle);
	
	Settings settings;
	
	public PlayScreen(Region gameWindow) {
		super(rootElement, "PlayScreen", gameWindow);
	}

	public Settings settings()
	{
		if(settings == null)
		{
			settings = new Settings(gameWindow);
		}
		return settings;
	}
	
	public void clickPlay(classes playerClass)
	{
		SikuliWrap.hoverRandomLocation(FileUtils.getPattern(imagesFolder, Images.playPlayButton), gameWindow);
		
		try {
			Pattern pattern = new Pattern(FileUtils.getPattern(imagesFolder, ClassMapper.getHeroDeckUnselected(playerClass))).similar(.92f);
			if(SikuliWrap.exists(gameWindow, pattern) != null)
			{
				SikuliDrawUtils.DrawRectangleAroundRegion(gameWindow.getLastMatch().x + 10, 
						gameWindow.getLastMatch().y + 20, gameWindow.getLastMatch().w - 20,
						gameWindow.getLastMatch().h, 1);
				gameWindow.click(RandomUtils.getRandomLocation(gameWindow.getLastMatch().x + 10, 
						gameWindow.getLastMatch().y + 20, gameWindow.getLastMatch().w - 20,
						gameWindow.getLastMatch().h));
			}
		} catch (FindFailed e) {
			Framework.Validate.fail("Could not select deck");
		}
		
		Match match = SikuliWrap.exists(gameWindow, FileUtils.getPattern(imagesFolder, Images.mode));
		if(match != null)
		{
			Pattern pattern = new Pattern(FileUtils.getPattern(imagesFolder, Images.modeUnselected)).similar(.95f);
			if(SikuliWrap.exists(gameWindow, pattern) != null)
			{
				SikuliWrap.clickLocation(RandomUtils.getRandomLocation(gameWindow.getLastMatch().x + 40 , 
						gameWindow.getLastMatch().y + 40, gameWindow.getLastMatch().w - 40,
						gameWindow.getLastMatch().h), gameWindow);
			}
		}	
		
		SikuliWrap.clickRandomLocation(FileUtils.getPattern(imagesFolder, Images.playPlayButton), gameWindow);
	}
	
	public void clickBackButton()
	{
		SikuliWrap.clickRandomLocation(FileUtils.getPattern(imagesFolder, Images.playBackButton), gameWindow);
	}
}

package Tests.Modules;

import org.junit.Test;
import org.sikuli.basics.Settings;
import org.sikuli.script.Region;
import org.sikuli.script.TextRecognizer;

import FrameworkHearthStone.Utilities.WindowUtils;

public class TextReaderTest {
	Region gameWindow = WindowUtils.getGameWindow();

	@Test
	public void getAllTextInWindow() {
		Settings.OcrTextSearch = true;
		Settings.OcrTextRead = true;
		Settings.OcrLanguage = "eng";
		TextRecognizer.reset();
		String text = gameWindow.text();
		Framework.Logger.log(text);
	}
}

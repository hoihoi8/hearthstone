package FrameworkHearthStone.Modules;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.UUID;

import org.sikuli.script.FindFailed;
import org.sikuli.script.Match;
import org.sikuli.script.Pattern;
import org.sikuli.script.Region;

import Framework.Logger;
import Framework.SikuliWrap;
import FrameworkHearthStone.DataStructures.CardData;
import FrameworkHearthStone.DataStructures.CardEncyclopedia;
import FrameworkHearthStone.DataStructures.Enums.classes;
import FrameworkHearthStone.DataStructures.GameData;
import FrameworkHearthStone.Utilities.CardUtils;
import FrameworkHearthStone.Utilities.ClassMapper;
import FrameworkHearthStone.Utilities.FileUtils;
import FrameworkHearthStone.Utilities.Images;
import FrameworkHearthStone.Utilities.ListUtils;
import FrameworkHearthStone.Utilities.RandomUtils;
import FrameworkHearthStone.Utilities.ScreenshotUtils;
import FrameworkHearthStone.Utilities.SikuliDrawUtils;

public class HandManager
{
	public CardEncyclopedia cardEncyclopedpia;
	public HashMap<String, Match> cardsInHandManaSymbols = new HashMap<String, Match>();
	HashMap<String, CardData> cardsInHand = new HashMap<String, CardData>(); //TODO make this not global, bad Josh
	private BoardIdleManager boardIdleManager;
	
	public HandManager(CardEncyclopedia cardEncyclopedpia, Region gameWindow)
	{
		this.cardEncyclopedpia = cardEncyclopedpia;
		boardIdleManager = new BoardIdleManager(gameWindow);
	}
	
	public HashMap<String, CardData> getHand(GameData gameData)
	{
		Logger.log("getHand");
		cardsInHand.clear();
		findCardManaSymbolsBasedOnImages(gameData, CardUtils.getBlankCard());
		MapCardDataToCardsInHand(gameData);
		cardsInHand.put(ClassMapper.getHeroPowerForHand(gameData).getNameWithGuid(), ClassMapper.getHeroPowerForHand(gameData));
		boardIdleManager.setMouseToIdle(false);
		return cardsInHand;
	}
	
	public HashMap<String, CardData> getHandUntilCardFound(GameData gameData, CardData card)
	{
		Logger.log("getHandUntilCardFound");
		findCardManaSymbolsBasedOnImages(gameData, card);
		MapCardDataToCardsInHand(gameData, card);
		cardsInHand.put(ClassMapper.getHeroPowerForHand(gameData).getNameWithGuid(), ClassMapper.getHeroPowerForHand(gameData));
		boardIdleManager.setMouseToIdle(false);
		return cardsInHand;
	}

	private HashMap<String, Match> findCardManaSymbolsBasedOnImages(GameData gameData, CardData card)
	{
		cardsInHandManaSymbols.clear();
		Iterator<Match> matches = null;
		List<Match> matchesList = null;
		
		for(int i=0; i<Images.cardCostHandRotationVariations.length; i++)
		{
			if(((Integer.parseInt(getNumeralOfManaCostFileName(Images.cardCostHandRotationVariations[i])) <= gameData.mana + 1) && card.getName().equals(""))
					|| (!card.getName().equals("") && (Integer.parseInt(getNumeralOfManaCostFileName(Images.cardCostHandRotationVariations[i])) == Integer.parseInt(card.manaCost))))
			{
				try {
					matches = gameData.handRegion.findAll(FileUtils.getPattern(Images.cardsHandFolder, Images.cardCostHandRotationVariations[i]));
					matchesList = ListUtils.copyIteratorToList(matches);
					for(Match match : matchesList)
					{
						addCardToHandIfNotDuplicate(Images.cardCostHandRotationVariations[i], match);
					}
				} catch (FindFailed e) {}
			}
			
			Framework.Validate.isTrue("Error: Hand is larger than ten. Size:" + Integer.toString(cardsInHandManaSymbols.entrySet().size()), cardsInHandManaSymbols.entrySet().size() <= 10);
		}
		
		return cardsInHandManaSymbols;
	}
	
	private void addCardToHandIfNotDuplicate(String cardCost, Match newMatch)
	{
		if(cardsInHandManaSymbols.size() == 0)
		{
			addToCardsInHand(cardCost, newMatch);
		}
		else
		{
			boolean duplicateCard = false;
			List <Match> tempCardsInHand = ListUtils.copyHashValueToList(cardsInHandManaSymbols);
			for(Match match : tempCardsInHand)
			{
				if(Math.abs(newMatch.x - match.x) < 10)
				{
					duplicateCard = true;
				}
			}
			if(!duplicateCard)
			{
				addToCardsInHand(cardCost, newMatch);
			}
		}
	}

	private void addToCardsInHand(String cardCost, Match newMatch) {
		String uniqueID = UUID.randomUUID().toString();
		cardsInHandManaSymbols.put(cardCost + uniqueID, newMatch);
		Logger.log("Card Found: " + cardCost + uniqueID + " " + newMatch.getScore());
	}
	
	private void MapCardDataToCardsInHand(GameData gameData)
	{
		MapCardDataToCardsInHand(gameData, CardUtils.getBlankCard());
	}
	
	private void MapCardDataToCardsInHand(GameData gameData, CardData targetCard)
	{
		Framework.Logger.log("MapCardDataToCardsInHand");
		for	(Entry<String, Match> entry : cardsInHandManaSymbols.entrySet())
		{
			boolean cardFound = false;
			try {
				gameData.handRegion.hover(RandomUtils.getRandomLocation(entry.getValue()));
				Region hoveredCard = Region.create(entry.getValue().x - 65, entry.getValue().y - 315, 140, 250);
				hoveredCard.setAutoWaitTimeout(0);
				SikuliDrawUtils.DrawRectangleAroundRegion(hoveredCard);
				
				for(CardData cardData : cardEncyclopedpia.cardDictionary.values())
				{
					if(cardData.cardClass == gameData.playerClass || cardData.cardClass == classes.neutral)
					{
						Pattern hover = FileUtils.getPattern(Images.cardsBigFolder, cardData.cardArtBigImage).similar(.8f);
						if(SikuliWrap.exists(hoveredCard, hover) != null)
						{
							Framework.Logger.log("Card found in hand: " + cardData.getName());
							cardData.match = entry.getValue();
							cardsInHand.put(cardData.getNameWithGuid(), cardData);
							cardFound = true;
							if(cardData.getName().equals(targetCard.getName())) {
								return;
							}
							else {
								break;
							}
						}
					}
				}
				if(!cardFound)
				{
					Framework.Logger.log("Could not find card in Encyclopedia.");
				}
			} catch (FindFailed e) {
				Framework.Logger.log("Unable to find card in dictionary");
			}
			
			if(cardFound == false)
			{
				addUnknownCard(entry);
			}
		}
	}

	private void addUnknownCard(Entry<String, Match> entry) {
		CardData genericCard = new CardData();
		genericCard.manaCost = getNumeralOfManaCostFileName(entry);
		cardsInHand.put("unknown", genericCard);
		ScreenshotUtils.takeScreenshot("UnkownCard");
	}

	private String getNumeralOfManaCostFileName(Entry<String, Match> entry) {
		//TODO doesn't work for 10+ cost cards
		return entry.getKey().toString().substring(0, 1);
	}
	
	private String getNumeralOfManaCostFileName(String entry) {
		//TODO doesn't work for 10+ cost cards
		return entry.substring(0, 1);
	}
}

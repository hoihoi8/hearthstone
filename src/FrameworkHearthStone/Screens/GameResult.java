package FrameworkHearthStone.Screens;

import org.sikuli.script.Pattern;
import org.sikuli.script.Region;

import Framework.SikuliWrap;
import FrameworkHearthStone.Utilities.FileUtils;
import FrameworkHearthStone.Utilities.Images;

public class GameResult extends BaseScreen
{
	private static final String imagesFolder = Images.GameResultFolder;
	private static Pattern rootElement = FileUtils.getPattern(imagesFolder, Images.gameResultRoot);

	public GameResult(Region gameWindow) {
		super(rootElement, "GameResult", gameWindow);
	}
	
	public void handleGameResult() {
		Framework.Logger.log("Handle Game Result");
		Pattern gameResultOK = FileUtils.getPattern(imagesFolder, Images.gameResultOK);
		if(SikuliWrap.exists(gameWindow, gameResultOK) != null)
		{
			SikuliWrap.clickRandomLocation(gameResultOK, gameWindow);
		}
	}
}
package Tests;

import FrameworkHearthStone.Hearthstone;

public class BaseTest {
	Hearthstone hearthstone;
	
	public Hearthstone hearthstone()
	{
		if(hearthstone == null)
		{
			hearthstone = new Hearthstone();
		}
		
		return hearthstone;
	}
}

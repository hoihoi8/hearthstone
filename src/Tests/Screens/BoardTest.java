package Tests.Screens;

import org.junit.Test;
import org.sikuli.script.Region;

import Framework.SikuliWrap;
import FrameworkHearthStone.DataStructures.Enums.classes;
import FrameworkHearthStone.Screens.Board;
import FrameworkHearthStone.Utilities.FileUtils;
import FrameworkHearthStone.Utilities.Images;
import FrameworkHearthStone.Utilities.WindowUtils;

public class BoardTest {
	private Region gameWindow = WindowUtils.getGameWindow();
	private Board board = new Board(classes.hunter, gameWindow);
	
	@Test
	public void highlightPlayerBoardRegion()
	{
		//board.populateGameData();
		//board.populateGameData();
		
		//board.getPlayerSecretRegion();
		//board.getGameOverRegion();
		//board.getTauntTextRegion();
		//board.getOpponentsTauntRegion();
		//board.getPlayerWeaponRegion();
		//board.getHeroPowerRegion();
		//board.getOpponentsFaceMatch();
		//board.getOpponentsBoardRegion();
		//board.getHeroPowerRegion();
		//board.getTurnButtonRegion();
		//board.playOneTurn();
		//board.getPlayerBoardRegion();
		//board.getOpponentsBoardRegion();
	}
//	@Test
//	public void highlightManaRegion()
//	{
//		Framework.Logger.log(Integer.toString(board.getAvailableMana()));
////	}
//	@Test
//	public void ClickEndTurnButton() {
//		board.clickTurnDone();
//	}
	
//	@Test
//	public void ClickHeroPowerButton()
//	{
//		board.clickHeroPower();
//	}
	
//	@Test
//	public void isTurnGreenTest()
//	{
//		board.isEndTurnButtonGreen();
//	}
	@Test
	public void gameNotOver()
	{
		boolean isGameOver = false;
		if(SikuliWrap.exists(gameWindow, FileUtils.getPattern(Images.playScreenFolder, Images.playTitle)) !=  null ||
				SikuliWrap.exists(gameWindow, FileUtils.getPattern(Images.practiceChooseDeckFolder, Images.practiceChooseDeckTitle)) != null)
		{
			Framework.Logger.log("Play or Choose Deck screen found. Ending game.");
			isGameOver = true;
		}
		
		Framework.Validate.isFalse("fail", isGameOver);
	}
}

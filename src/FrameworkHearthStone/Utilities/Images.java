package FrameworkHearthStone.Utilities;

public class Images
{
	public static String rootLocation = System.getProperty("user.dir");
	public static final String cardsBoardFolder = "\\Game\\cardsBoard\\";
	public static final String cardsBigFolder = "\\Game\\CardsBig\\";
	public static final String cardsHandFolder = "\\Game\\CardsHand\\";
	public static final String boardFolder = "\\Game\\Board\\";
	public static final String mulliganFolder = "\\Game\\Mulligan\\";
	public static final String playScreenFolder = "\\Game\\Play\\";
	public static final String practiceChooseDeckFolder = "\\Game\\PracticeChooseDeck\\";
	public static final String practiceChooseAdventureFolder = "\\Game\\PracticeChooseAdventure\\";
	public static final String questLogFolder = "\\Game\\QuestLog\\";
	public static final String mainMenuFolder = "\\Game\\MainMenu\\";
	public static final String seasonRewardsFolder = "\\Game\\SeasonRewards\\";
	public static final String GameResultFolder = "\\Game\\GameResult\\";
	public static final String uiFolder = "\\Game\\UI\\";
	public static final String globalFolder = "\\Game\\Global\\";
	public static final String gameHUDFolder = "\\Game\\gameHUD\\";
	public static final String settingsFolder = "\\Game\\settings\\";
	public static final String todaysQuestsFolder = "\\Game\\TodaysQuests\\";
	//TODO add zero cost?
	public static final String oneCostCardBig = "1cost.png";
	public static final String twoCostCardBig = "2cost.png";
	public static final String threeCostCardBig = "3cost.png";
	public static final String fourCostCardBig = "4cost.png";
	public static final String fiveCostCardBig = "5cost.png";
	public static final String sixCostCardBig = "6cost.png";
	public static final String sevenCostCardBig = "7cost.png";
	public static final String[] cardCostListBig = { oneCostCardBig, twoCostCardBig, threeCostCardBig, fourCostCardBig, fiveCostCardBig, sixCostCardBig, sevenCostCardBig };
	
	//TODO take screenshots for each card in each position
	public static final String threeCostCardHandNoTilt = "3Hand.png";
	public static final String threeCostCardHandTiltRight = "3costCardTiltRight.png";
	public static final String threeCostCardHandTiltLeft = "3costCardTiltLeft.png";
	
	public static final String[] cardCostHandRotationVariations = {
		"0Cost05Left.png", "0Cost10Right.png", "0Cost15Right.png", "0Cost20Right.png", "0Cost25Right.png",
		"0Cost05Left.png", "0Cost10Left.png", "0Cost15Left.png", "0Cost20Left.png", "0Cost25Left.png",
		
		"1Cost05Left.png", "1Cost10Right.png", "1Cost15Right.png", "1Cost20Right.png", "1Cost25Right.png",
		"1Cost05Left.png", "1Cost10Left.png", "1Cost15Left.png", "1Cost20Left.png", "1Cost25Left.png",
		
		"2Cost05Left.png", "2Cost10Right.png", "2Cost15Right.png", "2Cost20Right.png", "2Cost25Right.png",
		"2Cost05Left.png", "2Cost10Left.png", "2Cost15Left.png", "2Cost20Left.png", "2Cost25Left.png",
		
		"3Cost05Left.png", "3Cost10Right.png", "3Cost15Right.png", "3Cost20Right.png", "3Cost25Right.png",
		"3Cost05Left.png", "3Cost10Left.png", "3Cost15Left.png", "3Cost20Left.png", "3Cost25Left.png", 
		
		"4Cost05Left.png", "4Cost10Right.png", "4Cost15Right.png", "4Cost20Right.png", "4Cost25Right.png",
		"4Cost05Left.png", "4Cost10Left.png", "4Cost15Left.png", "4Cost20Left.png", "4Cost25Left.png",
		
		"5Cost05Left.png", "5Cost10Right.png", "5Cost15Right.png", "5Cost20Right.png", "5Cost25Right.png",
		"5Cost05Left.png", "5Cost10Left.png", "5Cost15Left.png", "5Cost20Left.png", "5Cost25Left.png",
	};
	
	//MAIN MENU
	public static final String mainMenuRootElement = "rootElement.png";
	public static final String questLogButton = "questLog.PNG";
	public static final String soloAdventuresButton = "SoloAdventures.PNG";
	//SEASON REWARDS
	public static final String seasonRewardsNext = "endOfSeasonNext.PNG";
	public static final String seasonRewardsDone = "endOfSeasonDone.PNG";
	//GAME RESULT
	public static final String gameResultOK = "OK.PNG";
	public static final String gameResultRoot = "gameResultRoot.PNG";
	//TODAYS QUESTS
	public static final String todaysQuests = "todaysQuests.PNG";
	//QUEST LOG
	public static final String questLogRootElement = "rootElement.png";
	public static final String questLogExit = "exit.png";
	//QUESTS
	public static final String cast40Spells = "cast40Spells.PNG";
	public static final String Deal100DamageToEnemyHeroes = "Deal100DamageToEnemyHeroes.PNG";
	public static final String Destroy40Minions = "Destroy40Minions.PNG";
	public static final String play20MinionsThatCost5OrMore = "play20MinionsThatCost5OrMore.PNG";
	public static final String watchAFriendWinInSpectatorMode = "watchAFriendWinInSpectatorMode.PNG";
	public static final String win2GamesWithDruidOrHunter = "win2GamesWithDruidOrHunter.PNG";
	public static final String win2GamesWithHunterOrMage = "win2GamesWithHunterOrMage.PNG";
	public static final String win2GamesWithMageOrShaman = "win2GamesWithMageOrShaman.PNG";
	public static final String win2GamesWithPaladinOrPriest = "win2GamesWithPaladinOrPriest.PNG";
	public static final String win2GamesWithPaladinOrWarrior = "win2GamesWithPaladinOrWarrior.PNG";
	public static final String win2GamesWithPriestOrWarlock = "win2GamesWithPriestOrWarlock.PNG";
	public static final String win2GamesWithRogueOrWarrior = "win2GamesWithRogueOrWarrior.PNG";
	public static final String win2GameWithShamanOrWarlock = "win2GamesWithShamanOrWarlock.PNG";
	public static final String Win3GamesWithAnyClass = "Win3GamesWithAnyClass.PNG";
	public static final String win5GamesWithHunterOrMage = "win5GamesWithHunterOrMage.PNG";
	public static final String win5GamesWithRogueOrWarrior = "win5GamesWithRogueOrWarrior.PNG";
	public static final String[] questList = { cast40Spells, Deal100DamageToEnemyHeroes, Destroy40Minions, 
		win2GamesWithDruidOrHunter, win2GamesWithHunterOrMage, win2GamesWithMageOrShaman,
		win2GamesWithPaladinOrPriest, win2GamesWithPaladinOrWarrior, win2GamesWithPriestOrWarlock, win2GamesWithRogueOrWarrior,
		win2GameWithShamanOrWarlock, Win3GamesWithAnyClass, win5GamesWithHunterOrMage, win5GamesWithRogueOrWarrior};
	public static final String[] ditchQuestList = { play20MinionsThatCost5OrMore, watchAFriendWinInSpectatorMode};
	public static final String quitQuest = "cancelQuest.png";
	//PLAY SCREEN
	public static final String playTitle = "playrootelement.png";
	public static final String hunterDeck = "hunterdeck.png";
	public static final String shamanDeck = "shamanDeck.png";
	public static final String palidanDeck = "palidanDeck.png";
	public static final String priestDeck = "priestDeck.png";
	public static final String rogueDeck = "rogueDeck.png";
	public static final String mageDeck = "mageDeck.png";
	public static final String warlockdeck = "warlockDeck.png";
	public static final String warriordeck = "warriorDeck.png";
	public static final String druidDeck = "druidDeck.png";
	public static final String hunterDeckUnselected = "hunterdeckUnselected.png";
	public static final String palidanDeckUnselected = "palidanDeckUnselected.png";
	public static final String shamanDeckUnselected = "shamanDeckUnselected.png";
	public static final String priestDeckUnselected = "priestDeckUnselected.png";
	public static final String rogueDeckUnselected = "rogueDeckUnselected.png";
	public static final String mageDeckUnselected = "mageDeckUnselected.png";
	public static final String warlockDeckUnselected = "warlockDeckUnselected.png";
	public static final String warriorDeckUnselected = "warriorDeckUnselected.png";
	public static final String druidDeckUnselected = "druidDeckUnselected.png";
	public static final String mode = "mode.png";
	public static final String modeUnselected = "modeUnselected.png";
	public static final String playPlayButton = "playbutton.png";
	public static final String playBackButton = "playScreenBackButton.png";
	//GAME HUD
	public static final String settingsButton = "settingsButton.png";
	//SETTINGS
	public static final String quitGameButton = "quitButton.PNG";
	//PRACTICE CHOOSE DECK SCREEN
	public static final String practiceChooseDeckTitle = "rootElement.png";
	public static final String deckChooseButton = "choose.png";
	public static final String practicePlayButton = "play.png";
	public static final String hunterAI = "hunter.png";
	public static final String rogueAI = "rogue.png";
	public static final String shamanAI = "shaman.png";
	public static final String lockedHeroAI = "lockedHero.PNG";
	//PRACTICE CHOOSE ADVENTURE
	public static final String practiceChooseAdventure = "chooseAdventureRoot.PNG";
	public static final String normalButton = "normalButton.PNG";
	public static final String expertButton = "expertButton.PNG";
	public static final String adventureChooseButton = "choose.png";
	//MULLIGAN SCREEN
	public static final String ConfirmButton = "confirmButton.png";
	public static final String waiting = "waiting.png";
	//BOARD SCREEN
	public static final String hunterHero = "hunterHero.png";
	public static final String druidHero = "druidHero.png";
	public static final String mageHero = "mageHero.png";
	public static final String palidanHero = "palidanHero.png";
	public static final String priestHero = "priestHero.png";
	public static final String rougeHero = "RougeHero.png";
	public static final String shamanHero = "shamanHero.png";
	public static final String warlockHero = "warlockHero.png";
	public static final String warriorHero = "warriorHero.png";
	public static final String[] heroPortraits = {
		hunterHero,druidHero,mageHero,palidanHero,priestHero,rougeHero,shamanHero,warlockHero,warriorHero
	};
	public static final String manaCrystal = "manaCrystal.png";
	public static final String windowedTitleBar2 = "windowedTitleBar2.png";
	public static final String endTurnGreenButton = "endTurnGreenButton.png";
	public static final String endTurnGreenKnaxxButton = "endTurnGreenKnaxxButton.png";
	public static final String endTurnButton = "endTurnButton.png";
	public static final String endTurnKnaxxButton = "endTurnGreenKnaxxButton.png";
	public static final String hunterHeroPowerButton = "hunterHeroPower.png";
	public static final String palidanHeroPowerButton = "palidanHeroPower.png";
	public static final String shamanHeroPowerButton = "shamanHeroPower.png";
	public static final String priestHeroPowerButton = "priestHeroPower.png";
	public static final String rogueHeroPowerButton = "rogueHeroPower.png";
	public static final String mageHeroPowerButton = "mageHeroPower.png";
	public static final String warlockHeroPowerButton = "warlockHeroPower.png";
	public static final String warriorHeroPowerButton = "warriorHeroPower.png";
	public static final String druidHeroPowerButton = "druidHeroPower.png";
	public static final String heroPowerUsed = "heroPowerUsed.png";
	public static final String enemyTurnButton = "enemyTurnButton.png";
	public static final String cardAttackUniversal = "cardAttackUniversal.PNG";
	public static final String cardAttackUniversalTaunt = "cardAttackUniversalTaunt.PNG";
	public static final String cardAttackSilenced = "cardAttackUniveralSilenced.PNG";
	public static final String heroWeaponAttack = "heroWeaponAttack.png";
	public static final String Victory = "Victory.png";
	public static final String Defeat = "Defeat.png";
	public static final String Reward = "Reward.png";
	public static final String clickToContinue = "ClickToContinue.png";
	public static final String goldEarned = "goldEarned.png";
	//TAUNTS
	public static final String tauntCreature = "taunt.png";
	public static final String tauntCreatureWithDivineShield = "tauntDivineShield.PNG";
	public static final String deathrattleTaunt = "deathrattleTaunt.png";
	public static final String deathrattleTauntDivineShield = "deathrattleTauntDivineShield.png";
	public static final String triggerTaunt = "triggerTaunt.png";
	public static final String triggerTauntDivineShield = "triggerTauntDivineShield.png";
	public static final String tauntCreatureWithZ1 = "tauntWithZ1.png";
	public static final String tauntCreatureWithZ2 = "tauntWithZ2.png";
	public static final String tauntWithGreen = "tauntWithGreen.png";
	public static final String goldenTaunt = "goldenTaunt.png";
	public static final String goldenTauntDivineShield = "goldentauntdivineshield.png";
	public static final String tauntUpperLeft = "tauntUpperLeft.png";
	public static final String tauntDivineShieldUpperLeft = "tauntDivineShieldUpperLeft.png";
	public static final String goldentauntdivineshieldUpperLeft = "goldentauntdivineshieldUpperLeft.png";
	public static final String tauntFrozen = "tauntFrozen.png";
	public static final String malganistaunt = "malganis.png";
	public static final String tauntInspire = "tauntInspire.png";
	public static final String tauntInspireDivineShield = "tauntInspireDivineShield.png";
	public static final String tauntText = "tauntText.png";
	
	public static final String secret = "secret.png";
}

package FrameworkHearthStone.Modules;

import java.util.ArrayList;
import java.util.List;

import org.sikuli.script.FindFailed;
import org.sikuli.script.Region;

import FrameworkHearthStone.Utilities.RandomUtils;
import FrameworkHearthStone.Utilities.SikuliDrawUtils;

public class BoardIdleManager {
	Region gameWindow;
	Region nwIdleRegion;
	Region neIdleRegion;
	Region swIdleRegion;
	Region seIdleRegion;
	List<Region> idleRegions = new ArrayList<Region>();
	
	public BoardIdleManager(Region gameWindow)
	{
		this.gameWindow = gameWindow;
	}
	
	public void setMouseToIdle()
	{
		setMouseToIdle(true);
	}
	public void setMouseToIdle(boolean rightClick)
	{
		if(idleRegions.size() == 0)
		{
			setupIdleRegions();
		}
		Region idle = idleRegions.get(RandomUtils.getRandomInt(idleRegions.size()));
		try {
			idle.hover(RandomUtils.getRandomLocation(idle));
			if(rightClick) {
				idle.rightClick();
			}
		} catch (FindFailed e) {
			Framework.Validate.fail("Unable to hover in idle region");
		}
	}
	
	private void setupIdleRegions() {
		getNwIdleRegion();
		getNeIdleRegion();
		getSwIdleRegion();
		getSeIdleRegion();
		idleRegions.add(nwIdleRegion);
		idleRegions.add(neIdleRegion);
		idleRegions.add(swIdleRegion);
		idleRegions.add(seIdleRegion);
	}

	public void idleActionsDuringOpponentsTurn()
	{
		//TODO do cool stuff like making the kite fly away
	}
	
	public Region getNwIdleRegion()
	{
		if(nwIdleRegion == null)
		{
			nwIdleRegion = Region.create(gameWindow.x + 40, gameWindow.y + 55, 330, 190);
			SikuliDrawUtils.DrawRectangleAroundRegion(nwIdleRegion);
			nwIdleRegion.setAutoWaitTimeout(0);			
		}
		
		return nwIdleRegion;
	}
	
	public Region getNeIdleRegion()
	{
		if(neIdleRegion == null)
		{
			neIdleRegion = Region.create(gameWindow.x + 710, gameWindow.y + 55, 280, 190);
			SikuliDrawUtils.DrawRectangleAroundRegion(neIdleRegion);
			neIdleRegion.setAutoWaitTimeout(0);
		}
		
		return neIdleRegion;
	}
	
	public Region getSwIdleRegion()
	{
		if(swIdleRegion == null)
		{
			swIdleRegion = Region.create(gameWindow.x + 20, gameWindow.y + 580, 290, 125);
			SikuliDrawUtils.DrawRectangleAroundRegion(swIdleRegion);
			swIdleRegion.setAutoWaitTimeout(0);
		}
		
		return swIdleRegion;
	}
	
	public Region getSeIdleRegion()
	{
		if(seIdleRegion == null)
		{
			seIdleRegion = Region.create(gameWindow.x + 710, gameWindow.y + 580, 290, 115);
			SikuliDrawUtils.DrawRectangleAroundRegion(seIdleRegion);
			seIdleRegion.setAutoWaitTimeout(0);
		}
		
		return seIdleRegion;
	}
}

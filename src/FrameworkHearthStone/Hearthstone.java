package FrameworkHearthStone;

import FrameworkHearthStone.Actions.Actions;
import FrameworkHearthStone.ProcessManagement.AccountConfigurations;
import FrameworkHearthStone.Screens.Screens;

public class Hearthstone {
	Screens screens;
	Actions actions;
	AccountConfigurations accounts = new AccountConfigurations();
	
	public Screens screens()
	{
		if(screens == null)
		{
			screens = new Screens();
		}
		return screens;
	}
	
	public Actions actions()
	{
		if(actions == null)
		{
			actions = new Actions();
		}
		return actions;
	}
	
	public AccountConfigurations accounts()
	{
		if(accounts == null)
		{
			accounts = new AccountConfigurations();
		}
		return accounts;
	}
}

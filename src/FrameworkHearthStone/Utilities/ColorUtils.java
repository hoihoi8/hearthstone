package FrameworkHearthStone.Utilities;

import java.awt.AWTException;
import java.awt.Color;
import java.awt.Robot;

import org.sikuli.script.Location;
import org.sikuli.script.Match;

public class ColorUtils {
	public static int getBlueValue(Match match)
	{
		return getBlueValue(match, 0);
	}
	public static int getBlueValue(Match match, int yOffset)
	{
		int blueValue = 0;
		Robot robot;
		try {
			robot = new Robot();
			Location location = new Location(match.getCenter().x, match.getCenter().y + yOffset);
			SikuliDrawUtils.DrawRectangleAtPoint(location);
			Color color = robot.getPixelColor(location.x, location.y);
			blueValue = color.getBlue();
			Framework.Logger.log("Blue value: " + Integer.toString(blueValue));
		} catch (AWTException e) {
		}
		
		return blueValue;
	}
	
	public static int getGreenValue(Match match)
	{
		return getGreenValue(match, 0);
	}
	public static int getGreenValue(Match match, int yOffset)
	{
		int greenValue = 0;
		Robot robot;
		try {
			robot = new Robot();
			Location location = new Location(match.getCenter().x, match.getCenter().y + yOffset);
			SikuliDrawUtils.DrawRectangleAtPoint(location);
			Color color = robot.getPixelColor(location.x, location.y);
			greenValue = color.getGreen();
			Framework.Logger.log("Green value: " + Integer.toString(greenValue));
		}
		catch (AWTException e) { }
		
		return greenValue;
	}
	
	public static int getRedValue(Match match)
	{
		return getRedValue(match, 0);
	}
	public static int getRedValue(Match match, int yOffset)
	{
		int redValue = 0;
		Robot robot;
		try {
			robot = new Robot();
			Location location = new Location(match.getCenter().x, match.getCenter().y + yOffset);
			SikuliDrawUtils.DrawRectangleAtPoint(location);
			Color color = robot.getPixelColor(location.x, location.y);
			redValue = color.getRed();
			Framework.Logger.log("Red value: " + Integer.toString(redValue));
		}
		catch (AWTException e) { }
		
		return redValue;
	}
}

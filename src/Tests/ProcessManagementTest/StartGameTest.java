package Tests.ProcessManagementTest;

import org.junit.Test;

import FrameworkHearthStone.ProcessManagement.AccountConfigurations;
import FrameworkHearthStone.ProcessManagement.StartHearthstone;

public class StartGameTest {
	@Test
	public void StartGame()
	{
		AccountConfigurations accountConfigs = new AccountConfigurations();
		
		StartHearthstone.loadGame(accountConfigs.accounts.get(0).accountName, accountConfigs.accounts.get(0).password);
		StartHearthstone.closeBattleNet();
	}
}

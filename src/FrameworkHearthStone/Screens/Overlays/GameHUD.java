package FrameworkHearthStone.Screens.Overlays;

import org.sikuli.script.Pattern;
import org.sikuli.script.Region;

import Framework.SikuliWrap;
import FrameworkHearthStone.Screens.BaseScreen;
import FrameworkHearthStone.Utilities.FileUtils;
import FrameworkHearthStone.Utilities.Images;


public class GameHUD extends BaseScreen {
	private static final String imagesFolder = Images.gameHUDFolder;
	private static Pattern rootElement = FileUtils.getPattern(imagesFolder, Images.settingsButton);
	
	public GameHUD(Region gameWindow) {
		super(rootElement, "GameHUD", gameWindow);
	}
	
	public void clickSettingsButton()
	{
		Framework.Logger.log("clickSettingsButton");
		Pattern settingsButton = FileUtils.getPattern(imagesFolder, Images.settingsButton);
		SikuliWrap.clickRandomLocation(settingsButton, gameWindow);
	}
}